import argparse
import numpy as np
import pickle
import os
import csv
import sys
from itertools import *
import workvariables as wv
import time
from scipy import sparse
import xgboost as xgb
import random
import collections
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import matthews_corrcoef
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score

def find_domain_of_concepts():
    '''
    This function reads the concept file and creates a dictionary with concept_id as
    key and domain_id as value.
    '''
    concept_id_domain_id = {}
    inp_line_count = -1
    with open(wv.concept_file) as inp:
        for line in inp:
            inp_line_count+=1
            vals = line.rstrip().split('\t')
            if (inp_line_count == 0):
                conc_idx = vals.index('concept_id')
                domain_idx = vals.index('domain_id')
            else:
                concept_id_domain_id[vals[conc_idx]] = vals[domain_idx]
    print ('Set of domains in the concept file is.....{0}'.format(set(concept_id_domain_id.values())))
    return concept_id_domain_id


def determine_domain_of_covariates(concept_id_domain_id):
    '''
    There are two covariate files: one has all concepts present in 473m metavisits
    including their ancestors, another has only concepts. (CPT4 and ICD9Proc were
    converted to ICD10PCS)
    '''
    concept_pos_dom_anc = {}
    #concepts and ancestors
    with open(wv.all_covfile) as cov:
        for line in cov:
            concepts_anc = line.strip().split(',')
    #only concepts
    with open(wv.all_covfile1) as cov1:
        for line in cov1:
            concepts_no_anc = line.strip().split(',')

    ancestors = set(concepts_anc).difference(concepts_no_anc)
    # create a dictionary with concept_id as key and (pos,domain,anc/no_anc) as value.
    # pos = position of the concept in the list of covariates
    # domain = domain of the concept_id
    # anc/no_anc = 0 if the concept is not ancestor, 1 if ancestor.
    all_concepts = concept_id_domain_id.keys()
    for pos, concept in enumerate(concepts_anc):
        if concept in all_concepts:
            if (concept in ancestors):
                concept_pos_dom_anc[concept] = (pos, concept_id_domain_id[concept], 1)
            else:
                concept_pos_dom_anc[concept] = (pos, concept_id_domain_id[concept], 0)
        else:
            concept_pos_dom_anc[concept] = (pos, 'Generic', 0)
        pos+=1
    print ('Number of covariates in concept_pos_dom_anc dictionary: {0}, list of domains: {1}'.format(len(concept_pos_dom_anc), set([v[1] for v in concept_pos_dom_anc.values()])))
    return concept_pos_dom_anc

def powerset(domain_list):
    '''
    This function generates the powerset of the set of domains i.e. finds all possible
    combinations of the domains.
    '''
    s = list(domain_list)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

def get_command_line_args(allowed_domains):
    '''
    This function reads the arguments passed through command-line. The argument
    will have the list of domains that need to be selected for the classifier.
    Also, whether ancestor should be selected or not. If the command_line arguments
    are not provided, all allowed_domains will be used and ancestor will also be set to Y.
    '''
    if (len(sys.argv) > 1):
        # command-line parameters
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '--domain',
            help='Type domain name, such as  drug, observation, etc. If you want to mention multiple domains, put + between them. e.g. drug+observation')
        parser.add_argument(
            '--ancestor',
            choices=['Y', 'N'],
            help='Do you want to fetch ancestor concept ids too(Y/N)?')

        # store command-line arguments in local variables. if arguments are not passed, use default values
        # check if domains provided
        argument = parser.parse_args()
        if (argument.domain):   #check domains
            domains = argument.domain
        else:
            print ('No domain name was provided, so the program cannot run.')
            exit()

        # check if user misspelled any domain name
        d_list = domains.strip().split('+')
        for d in d_list:
            if (d.title() not in allowed_domains):
                print ('Domain {0} is not allowed, so the program will stop. Allowed domains are {1}'.format(d, allowed_domains))
                exit()

        # check if ancestors need to be selected
        if (argument.ancestor):
            anc = argument.ancestor
        else:
            anc = 'N'
    else:   #no command-line arguments
        domains = '+'.join(v for v in allowed_domains)
        anc = 'Y'

    return domains, anc


def genereate_performance_matrix(performance_data_dict, csvfile, fieldnames, anc, writer):
    '''
    This function generates performance matrix using the given dictionary.
    '''
    print ('Creating performance matrix file.....', fieldnames)
    #write data
    datarow = {}
    for k,v in performance_data_dict.items():
        for field in fieldnames:    # initialize dictionary
            if (field in k):
                datarow[field] = '1'
            else:
                datarow[field] = '0'
        if (anc == 'Y'):
            datarow['Ancestor'] = '1'
        else:
            datarow['Ancestor'] = '0'

        datarow['AUC'] = v[0]
        datarow['MCC'] = v[1]
        datarow['Accuracy'] = v[2]
        writer.writerow(datarow)
    print ('Record written to the CSV file')


def get_covariate_indexes(concepts, concept_pos_dom_anc):
    '''
    find the indices of the selected concepts in the list of covariates
    '''
    cov_idx = set()
    for concept in concepts:
        cov_idx.add(concept_pos_dom_anc[concept][0])
    return cov_idx


def create_data_label(selected_cols, total_row, total_col, co, matrix_indices):
    '''
    Create data for the classifier using selected_cols i.e. covariates.
    '''
    print ('Creating data and label for the combination of domains....')
    rownums = []
    colnums = []
    for (ro,co) in matrix_indices:
        if co in selected_cols:
            rownums.append(ro)
            colnums.append(co)

    data = np.array([1 for i in range(len(rownums))], dtype=np.int8)
    X = sparse.csr_matrix((data, (rownums, colnums)), shape=(total_row, total_col))
    print (X.__getattr__)

    # fetch all labels from the saved file
    with open (wv.bal_labels_file) as fl:
        for line in fl:
            Y = line.strip().split(',')
    Y = np.array(Y, dtype=np.int8)

    #fetch all metavisits from the saved file
    with open (wv.bal_metavisits_file) as fm:
        for line in fm:
            metavisits_list = line.strip().split(',')

    return X, Y, metavisits_list


def compute_classification_results(concept_pos_dom_anc, powerset_domain_list, anc, classification_results_text_file):
    '''
    This function first calls other functions to compute X (data) and Y (label) using the
    combination of domains present in powerset and then computes MCC and AUC for the classifier
    using computed X and Y.
    '''
    # create two dictionaries:
    domain_concept = {} # domain_id as key as set of concepts as value
    domain_concept_anc = {} #domain_id as key as set of ancestor concepts as value
    #print (len(concept_pos_dom_anc)) #188663
    for k, v in concept_pos_dom_anc.items():
        if (v[2] == 0):
            try:
                domain_concept[v[1]].add(k)
            except:
                domain_concept[v[1]] = set([k])
        elif(v[2] == 1):
            try:
                domain_concept_anc[v[1]].add(k)
            except:
                domain_concept_anc[v[1]] = set([k])
        else:
            print('Something is fishy!!!!')
            exit()

    #cross validation for number of concept ids
    tot = {}
    for k,v in domain_concept.items():
        tot[k]=len(v)
    for k,v in domain_concept_anc.items():
        try:
            tot[k]+=len(v)
        except:
            print(k,v)
    print(tot)

    # determine which concept ids need to be selected for each combinations present
    # in the powerset of domains.
    combo_concept_dict = {}
    for domain, concepts in domain_concept.items():
        for dlist in powerset_domain_list:
            if (domain in dlist):
                try:
                    combo_concept_dict[dlist].update(concepts)  #add all concepts of that domain
                except:
                    combo_concept_dict[dlist] = set(concepts)  #add all concepts of that domain
                if (anc == 'Y'):
                    combo_concept_dict[dlist].update(domain_concept_anc.get(domain,set()))    #add all ancestors of that domain
    # print the number of concepts in each combination of the domains
    for k, v in combo_concept_dict.items():
        print (k, len(v))

    # Recreate CSR matrix for each combination of domains and run classifier
    performance_data_dict = {} # to store mcc and auc for each combination of domains
    # load the saved compressed sparse matrix and compute number of rows and colnums
    #X_saved = np.load(wv.b_csr_file1).tolist()  #np.load converts csr matrix to object, that's why tolist is used.
    with open(wv.bal_csr_file, 'rb') as pif:
        X_saved = pickle.load(pif)  #use pickle instead of numpy load
    print (X_saved.__getattr__)
    total_row = X_saved.shape[0]
    total_col = X_saved.shape[1]
    # Which elements of the saved CSR matrix have value 1
    X_1 = (X_saved == 1)  #all elements with value 1
    co = X_1.tocoo() #Return a COOrdinate representation of this matrix
    matrix_indices = set(zip(co.row, co.col)) # create a list of (row, col) where the value is 1

    # for each combination of domains, find the indices of the concepts and use
    #those indices to create data for the classifier.
    for combo, concepts in combo_concept_dict.items():
        stime = time.time()
        print ('\nProcessing for domain combination.... {0}'.format(combo))
        selected_cols = get_covariate_indexes(concepts, concept_pos_dom_anc)
        X, Y, metavisits_list =  create_data_label(selected_cols, total_row, total_col, co, matrix_indices)
        classify_the_data (X, Y, metavisits_list)
        auc_val, mcc_val, acc_val = get_accuracy_auc_score()
        line = str(combo) + '..............' + '  AUC: ' + str(auc_val) + '  MCC: ' + str(mcc_val) + ' Accuracy: ' + str(acc_val) + '\n'
        classification_results_text_file.write(line)
        performance_data_dict[combo] = (auc_val,mcc_val,acc_val)
        print ('Combination: {0} Total time: {1} seconds'.format(combo, time.time()-stime))
    return performance_data_dict


def classify_the_data (X, Y, metavisits_list):
    '''
    This function calls xgboost classifier to classify the data and compute
    several performance metrices.
    '''
    #imp_features = {}
    mpf = open(wv.full_fact_metavisits_probs_file, 'w')   #file to store metavisits and their corresponding probabilty
    tot = collections.Counter(Y)
    print (tot)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    # compute indices for train and test data for 5-fold validation
    print ('Generating train and test indices for 5-fold validation....')
    v = np.random.permutation(X.shape[0])
    r5 = len(v) % 5
    i5 = 0
    for i in range(5):
        lv5 = int(len(v)/5)
        if(r5 > 0):
            lv5 = lv5+1
            r5 = r5-1
        teidx = v[i5:min(i5+lv5,len(v))]
        tridx = np.append(v[0:i5], v[min(i5+lv5,len(v)):len(v)])
        i5 = i5 + lv5

        print ('\nPerforming fold number {0}'.format(i+1))
        tr_time = time.time()

        #print ('Converting train and test data to Dmatrix....')
        #dtrain = xgb.DMatrix(X[tridx], label=Y[tridx])  #load train sparse matrix into Dmatrix
        #dtest = xgb.DMatrix(X[teidx], label=Y[teidx])   #load test sparse matrix into Dmatrix
        print ('Train the classifier....')
        #dbst = xgb.train(param, dtrain, num_round)
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric='auc', verbose=False)
        bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx])
        #bst = xgb.XGBClassifier().fit(X[tridx], Y[tridx])
        print ('Training time:', time.time()-tr_time)

        print ('Test the classifier....')
        te_time = time.time()
        #preds = dbst.predict(dtest)  # this will return probabilities
        preds = bst.predict_proba(X[teidx])  # this will return probabilities
        print ('Test time:', time.time()-te_time)

        #print ('Finding important features.............')
        #imps = dbst.get_score(importance_type='gain')
        #imps = bst.get_booster().get_score(importance_type='gain')
        #imp_features.update(imps)
        #print (imps)

        print ('Store the computed propabilities in a dictionary...')
        metavisit_suicide_prob = {}
        y_test = Y[teidx]
        for j in range(len(teidx)):
            metavisit_suicide_prob[teidx[j]] = (preds[j], y_test[j])
        save_results_in_a_file(metavisit_suicide_prob, metavisits_list, mpf)

    #close file and return values
    mpf.close()
    #return imp_features

def save_results_in_a_file(metavisit_suicide_self_harm_prob, metavisits_list, mpf):
    '''
    This function, using the variables metavisit_suicide_self_harm_prob and metavisits_list,
    generates record with metavisit & its probabilty and writes it to the file.
    '''
    # write metavisit and its probabilty to an output file
    print ('Writing metavisits and their corresponding probabilities to the file...')
    for mvisit, pred in metavisit_suicide_self_harm_prob.items():
        line = str(metavisits_list[mvisit]) + ' : ' + str(pred[0]) +  ' : ' + str(pred[1]) + '\n'
        mpf.write(line)

def get_accuracy_auc_score():
    '''
    This function computes the accuracy of the classifier.
    '''
    y_true = []
    y_pred_acc = []
    y_pred_auc = []
    with open(wv.full_fact_metavisits_probs_file) as mpf:
        for line in mpf:
            vals = line.strip().split(':')
            y_true.append(int(vals[2].strip()))
            prob_0 = float(vals[1].strip().strip('[').strip(']').split(' ')[0]) #probabilty for class 0
            prob_1 = 1-prob_0  #doing this stupid thing because of spaces in the string.
            y_pred_auc.append(prob_1)
            y_pred_acc.append(round(prob_1))
    #print (y_pred[:10])
    #print (y_true)
    # evaluate predictions
    print('Accuracy of the classifier is...............: {0}'.format(accuracy_score(y_true, y_pred_acc)))
    print('MCC of the classifier is....................: {0}'.format(matthews_corrcoef(y_true, y_pred_acc)))
    print('F1 Score of the classifier is...............: {0}'.format(f1_score(y_true, y_pred_acc, average='micro')))
    print('AUC for the classifier is...................: {0}'.format(roc_auc_score(y_true, y_pred_auc)))
    return roc_auc_score(y_true, y_pred_auc), matthews_corrcoef(y_true, y_pred_acc), accuracy_score(y_true, y_pred_acc)


def main ():
    '''
    This is the start of the program. FULL-FACTORIAL ANALYSIS
    '''
    print ('\nCreating concept_id_domain_id dictionary with concept as key and domain as value....')
    stime = time.time()
    concept_id_domain_id = find_domain_of_concepts()
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    print ('\nRead the covariate files and determine the domains of concepts and whether or not they are ancestors....')
    stime = time.time()
    concept_pos_dom_anc = determine_domain_of_covariates(concept_id_domain_id)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))
    #print (concept_pos_dom_anc['2814495'])

    print ('\nGet domains that need to be used for the classifier from command line...')
    stime = time.time()
    allowed_domains = set([v[1] for v in concept_pos_dom_anc.values() if v[1] != 'Spec Anatomic Site'])
    domains, anc = get_command_line_args(allowed_domains)
    #print (len(domains))
    print ('\nCreate the various combinations of selected domains...')
    domain_list = [domain.title() for domain in domains.strip().split('+')]
    powerset_domain_list = [elem for elem in list(powerset(domain_list)) if len(elem) > 0]  #discard empty set
    print ('Total combinations of domains are....{0}'.format(len(powerset_domain_list)))
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    print ('\nCompute Accuracy, MCC and AUC for each combination of domains....')
    stime = time.time()
    # for csv file
    fieldnames = domain_list + ['Ancestor', 'AUC', 'MCC', 'Accuracy']
    csvfile = open (wv.full_fact_matrix_file, 'w')
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    # for text file
    classification_results_text_file = open(wv.full_fact_classification_result_file, 'w') #file to store mcc_auc value
    line = 'Without selecting ancestors' + '\n'
    classification_results_text_file.write(line)
    line = '=============================' + '\n'
    classification_results_text_file.write(line)

    if (anc == 'Y'):
        print ('Ancestors needed, first compute performance parameters without ancestors....')
        anc_check = 'N'
        performance_data_dict = compute_classification_results(concept_pos_dom_anc, powerset_domain_list, anc_check, classification_results_text_file)   # without ancestors
        genereate_performance_matrix(performance_data_dict, csvfile, fieldnames, anc_check, writer)

        print ('\nAncestors needed, now compute performance parameters with ancestors....')
        anc_check = 'Y'
        line = '\n' + 'With ancestors' + '\n'
        classification_results_text_file.write(line)
        line = '=============================' + '\n'
        classification_results_text_file.write(line)
        performance_data_dict = compute_classification_results(concept_pos_dom_anc, powerset_domain_list, anc_check, classification_results_text_file)   #with ancestors
        genereate_performance_matrix(performance_data_dict, csvfile, fieldnames, anc_check, writer)
    else:
        print ('Ancestors not needed.....!!!!!!!!!!!!!!!!!!!!!!!!! ')
        performance_data_dict = compute_classification_results(concept_pos_dom_anc, powerset_domain_list, anc, classification_results_text_file)   # without ancestors
        genereate_performance_matrix(performance_data_dict, csvfile, fieldnames, anc, writer)

    classification_results_text_file.close()
    csvfile.close()
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

if __name__ == '__main__':
    '''
    This program selects the covariates of the domain passed as command-line argument and using only
    those covariates, computes the classification results (AUC, MCC, Accuracy) for the XGboost model
    on the balanced dataset.
    If multiple domains are passed, it generates the powerset of domains. Then, for each member in
    the powerset, it selects covariates and using only those covariates, computes the classification
    results (AUC, MCC, Accuracy) for the XGboost model on the balanced dataset.
    If you want to run this code for all domains, DO NOT pass anything as command-line argument.
    '''
    main()
