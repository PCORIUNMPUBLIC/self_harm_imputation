import workvariables as wv
import gzip
import time
import numpy as np
import pickle
from scipy import sparse

##############################################################################
##########################PHASE 1 Functions###################################
##############################################################################

def find_concept_ids_of_concept_codes():
    '''
    This function reads concept file and creates a dictionary with concept_code
    as key and the corresponding concept_id as value, set of ICD9CM/ICD10CM
    concept ids, set of SNOMED concept ids, set of CPT4 concept ids, and set
    of ICD9Proc concept ids.
    '''
    line_count = 0
    concept_code_id_dict = {}
    icd9_icd10cm_concepts_set = set()
    SNOMED_concepts_set = set()
    CPT4_concepts_set = set()
    ICD9Proc_concepts_set = set()

    with open(wv.concept_file) as inp1:
        for line in inp1:
            vals = line.rstrip().split('\t')
            if (line_count == 0):
                concept_idx = vals.index('concept_id')
                concept_code_idx = vals.index('concept_code')
                vocabulary_idx = vals.index('vocabulary_id')
                #print (concept_idx, concept_code_idx, vocabulary_idx)
                line_count+=1
            else:
                concept_code_id_dict[(vals[concept_code_idx],vals[vocabulary_idx])] = vals[concept_idx]
                if (vals[vocabulary_idx] == 'ICD9CM' or vals[vocabulary_idx] == 'ICD10CM'):
                    icd9_icd10cm_concepts_set.add(vals[concept_idx])
                elif (vals[vocabulary_idx] == 'CPT4'):
                    CPT4_concepts_set.add(vals[concept_idx])
                elif (vals[vocabulary_idx] == 'ICD9Proc'):
                    ICD9Proc_concepts_set.add(vals[concept_idx])
                elif (vals[vocabulary_idx] == 'SNOMED'):
                    SNOMED_concepts_set.add(vals[concept_idx])

    print ('Num of elements in concept_code_id_dict:', len(concept_code_id_dict))
    print ('Num of elements in ICD9_10CM set:', len(icd9_icd10cm_concepts_set))
    print ('Num of elements in SNOMED set:', len(SNOMED_concepts_set))
    print ('Num of elements in ICD9Proc set:', len(ICD9Proc_concepts_set))
    print ('Num of elements in CPT4 set:', len(CPT4_concepts_set))
    return concept_code_id_dict, icd9_icd10cm_concepts_set, SNOMED_concepts_set, CPT4_concepts_set, ICD9Proc_concepts_set


def find_icd10proc_for_icd9proc_and_cpt4_concept_ids(concept_code_id_dict):
    '''
    This function reads the spreadsheets comprising ICD9Proc/CPT concept codes and ICD10Proc
    concept codes. Using concept_code_id_dict, it finds the corresponding concept ids for
    the given concept codes and then creates a dictionary with ICD9Proc/CPT4 concept_id as key
    and ICD10Proc concept_ids as values. One ICD9Proc/CPT4 concept code might have mapping to
    several ICD10Proc concept codes.
    '''
    print ('Creating icd9_icd10_proc dictionary...')
    line_count = -1
    bad_rec_count = 0
    icd9_icd10_proc_dict = {}
    with open(wv.icd9_icd10Proc_file) as inp2:
        for line in inp2:
            line_count+=1
            vals = line.rstrip().split(',')
            if (line_count == 0):
                continue
            else:
                vals = [e.strip().strip("'").strip() for e in vals if e != '']
                if(len(vals)==1):
                    bad_rec_count+=1
                for i in range(1,len(vals)):
                    if ((vals[0],'ICD9Proc') in concept_code_id_dict and (vals[i],'ICD10PCS') in concept_code_id_dict):
                        if (concept_code_id_dict[(vals[0],'ICD9Proc')] in icd9_icd10_proc_dict):
                            icd9_icd10_proc_dict[concept_code_id_dict[(vals[0],'ICD9Proc')]].add(concept_code_id_dict[(vals[i],'ICD10PCS')])
                        else:
                            icd9_icd10_proc_dict[concept_code_id_dict[(vals[0],'ICD9Proc')]] = set([concept_code_id_dict[(vals[i],'ICD10PCS')]])
                    else:
                        bad_rec_count+=1
    print('Total records in icd9_icd10Proc_file..:{0}, Records added to dict..:{1}, Unprocessed records..:{2}'.format(line_count, len(icd9_icd10_proc_dict), bad_rec_count))

    print ('Creating CPT4_icd10_proc dictionary....')
    line_count = 0
    bad_rec_count = 0
    cpt4_icd10_proc_dict = {}
    with open(wv.cpt4_icd10_file) as inp3:
        for line in inp3:
            line_count+=1
            vals = line.rstrip().split('|')
            vals = [e.strip().strip("'").strip() for e in vals if len(e) >= 3]
            if ((vals[0],'CPT4') in concept_code_id_dict and (vals[1],'ICD10PCS') in concept_code_id_dict):
                if (concept_code_id_dict[(vals[0],'CPT4')] in cpt4_icd10_proc_dict):
                    cpt4_icd10_proc_dict[concept_code_id_dict[(vals[0],'CPT4')]].add(concept_code_id_dict[(vals[1],'ICD10PCS')])
                else:
                    cpt4_icd10_proc_dict[concept_code_id_dict[(vals[0],'CPT4')]] = set([concept_code_id_dict[(vals[1],'ICD10PCS')]])
            else:
                bad_rec_count+=1
    print('Total records in cpt4_icd10_file..:{0}, Records added to dict..:{1}, Unprocessed records..:{2}'.format(line_count, len(cpt4_icd10_proc_dict), bad_rec_count))
    return icd9_icd10_proc_dict, cpt4_icd10_proc_dict


def find_ancestors_of_concepts():
    '''
    This function reads concept_ancestor file and creates a dictionary with
    descendant_concept_id as key and ancestor_concept_id as values.
    '''
    line_count = 0
    descendant_ancestor_dict = {}
    with open(wv.concept_ancestor_file) as fin:
        for line in fin:
            vals = line.rstrip().split('\t')
            if (line_count == 0):
                ancestor_concept_idx = vals.index('ancestor_concept_id')
                descendant_concept_idx = vals.index('descendant_concept_id')
                line_count+=1
            else:
                ancestor_concept = vals[ancestor_concept_idx]
                descendant_concept = vals[descendant_concept_idx]
                if (descendant_concept in descendant_ancestor_dict):
                    descendant_ancestor_dict[descendant_concept].add(ancestor_concept)
                else:
                    descendant_ancestor_dict[descendant_concept] = set([ancestor_concept])
    return descendant_ancestor_dict


def find_covariates_and_label_for_metavisit(icd9_icd10cm_concepts_set, CPT4_concepts_set, ICD9Proc_concepts_set, SNOMED_concepts_set,
                                            descendant_ancestor_dict, icd9_icd10_proc_dict, cpt4_icd10_proc_dict):
    '''
    This function reads metavisit input files and determines the covariates and label for
    the metavisit.
    '''
    inp_line_count = -1
    out_line_count = 0
    prev_metavisit_occurrence_id = -1
    covariates_of_curr_metavisit = set() #all covariates of current metavisit
    covariates_of_curr_metavisit_orig = set()  #covariates of the current metavisit without mapping
    set_of_all_covariates = set()   #all covariates using all metavisits

    fo = open(wv.all_metavisits_cov_file,'w') #output file to keep all metavisits and their covariates

    with gzip.open(wv.SQLopfile, 'rb') as fin:
        for line in fin:
            #print (line)
            inp_line_count+=1
            vals = line.decode('utf8').rstrip().split('\t')
            if (inp_line_count == 0):
                continue
            else:
                curr_metavisit_occurrence_id = vals[0]
                concept_ids = vals[1].strip('{').strip('}').split(',')
                concept_set_name = vals[2]
                if (curr_metavisit_occurrence_id == prev_metavisit_occurrence_id or prev_metavisit_occurrence_id == -1):
                    prev_metavisit_occurrence_id = curr_metavisit_occurrence_id
                    covariates_of_curr_metavisit_orig.update(concept_ids)  #all concepts without mapping
                    covariates_of_curr_metavisit = update_covariates_of_curr_metavisit(vals, covariates_of_curr_metavisit, icd9_icd10cm_concepts_set,
                                                                            SNOMED_concepts_set, descendant_ancestor_dict, icd9_icd10_proc_dict,
                                                                            cpt4_icd10_proc_dict, CPT4_concepts_set, ICD9Proc_concepts_set)
                else:
                    #determine the label of the metavisit
                    if (len(covariates_of_curr_metavisit.intersection(wv.excluded_concepts)) > 0 or len(covariates_of_curr_metavisit_orig.intersection(wv.excluded_concepts)) > 0):
                        label = '1'
                    else:
                        label = '0'
                    #make sure no excluded covariate is present in the list of covariates.
                    if (len(covariates_of_curr_metavisit.intersection(wv.excluded_concepts)) > 0):  #excluded covariate found
                        covariates_of_curr_metavisit = covariates_of_curr_metavisit.difference(wv.excluded_concepts)
                        set_of_all_covariates.update(covariates_of_curr_metavisit)
                    else:
                        set_of_all_covariates.update(covariates_of_curr_metavisit)
                    str_of_covariates = ','.join(cov for cov in covariates_of_curr_metavisit)    #set to string
                    line = prev_metavisit_occurrence_id + ',' + label + ',' + str_of_covariates + '\n'
                    #print (line, len(covariates_of_curr_metavisit))
                    fo.write(line)
                    out_line_count+=1

                    #prepare for new metavisit
                    covariates_of_curr_metavisit_orig = set()
                    covariates_of_curr_metavisit = set()
                    prev_metavisit_occurrence_id = curr_metavisit_occurrence_id
                    covariates_of_curr_metavisit_orig.update(concept_ids)  #all concepts
                    covariates_of_curr_metavisit = update_covariates_of_curr_metavisit(vals, covariates_of_curr_metavisit, icd9_icd10cm_concepts_set,
                                                                            SNOMED_concepts_set, descendant_ancestor_dict, icd9_icd10_proc_dict,
                                                                            cpt4_icd10_proc_dict, CPT4_concepts_set, ICD9Proc_concepts_set)
                if (inp_line_count%2500000 == 0):
                    print ('{0} input records were processed and {1} distinct metavisits'.format(inp_line_count, out_line_count))


    # DO NOT Skip the last record. Determine the label of the metavisit
    if (len(covariates_of_curr_metavisit.intersection(wv.excluded_concepts)) > 0 or len(covariates_of_curr_metavisit_orig.intersection(wv.excluded_concepts)) > 0):
        label = '1'
    else:
        label = '0'
    #make sure no excluded covariate is present in the list of covariates.
    if (len(covariates_of_curr_metavisit.intersection(wv.excluded_concepts)) > 0):  #excluded covariate found
        covariates_of_curr_metavisit = covariates_of_curr_metavisit.difference(wv.excluded_concepts)
        set_of_all_covariates.update(covariates_of_curr_metavisit)
    else:
        set_of_all_covariates.update(covariates_of_curr_metavisit)
    str_of_covariates = ','.join(cov for cov in covariates_of_curr_metavisit)    #set to string
    line = prev_metavisit_occurrence_id + ',' + label + ',' + str_of_covariates + '\n'
    fo.write(line)
    out_line_count+=1
    fo.close()
    print ('All records were written....{0} input records were processed and {1} distinct metavisits'.format(inp_line_count, out_line_count))
    print ('Total number of covariates:', len(set_of_all_covariates))
    return set_of_all_covariates


def update_covariates_of_curr_metavisit(vals, covariates_of_curr_metavisit, icd9_icd10cm_concepts_set,
                                SNOMED_concepts_set, descendant_ancestor_dict, icd9_icd10_proc_dict,
                                cpt4_icd10_proc_dict, cpt4_concepts_set, icd9proc_concepts_set):
    '''
    This function adds concepts to the set of covariates of a metavisit.
    '''
    #print ('Updating set of covariates using each concepts found in each row of the input file...')
    concept_ids = vals[1].strip('{').strip('}').split(',')
    # add all concepts to covariates_of_curr_metavisit
    for concept_id in concept_ids:
        if (concept_id in cpt4_concepts_set):
            if (concept_id in cpt4_icd10_proc_dict):
                for conc in cpt4_icd10_proc_dict[concept_id]:
                    covariates_of_curr_metavisit.add(conc)
                    covariates_of_curr_metavisit.update(descendant_ancestor_dict.get(conc,set()))
                    #print ('CPT4- concept: {0} and ancestor:{1}'.format(conc, descendant_ancestor_dict.get(conc,set())))
            #else:
            #    print ('CPT44 ---',concept_id)
        elif(concept_id in icd9proc_concepts_set):
            if (concept_id in icd9_icd10_proc_dict):
                for conc in icd9_icd10_proc_dict[concept_id]:
                    covariates_of_curr_metavisit.add(conc)
                    covariates_of_curr_metavisit.update(descendant_ancestor_dict.get(conc,set()))
                    #print ('ICD-concept: {0} and ancestor:{1}'.format(conc, descendant_ancestor_dict.get(conc,set())))
            #else:
            #    print ('ICD9PRoc ---',concept_id)
        elif(concept_id in SNOMED_concepts_set):
            if (len(set(concept_ids).intersection(wv.excluded_concepts)) == 0):
                covariates_of_curr_metavisit.add(concept_id)
                covariates_of_curr_metavisit.update(descendant_ancestor_dict.get(concept_id,set()))
                #print ('SNOMED - concept: {0} and ancestor:{1}'.format(concept_id, descendant_ancestor_dict.get(concept_id,set())))
        elif(concept_id in icd9_icd10cm_concepts_set):
            continue
        else:
            covariates_of_curr_metavisit.add(concept_id)
            covariates_of_curr_metavisit.update(descendant_ancestor_dict.get(concept_id,set()))
            #print ('Others - concept: {0} and ancestor:{1}'.format(concept_id, descendant_ancestor_dict.get(concept_id,set())))

    #add concept_set_name to covariates_of_curr_metavisit
    concept_set_name = vals[2]
    if (concept_set_name != ''):
        covariates_of_curr_metavisit.add(concept_set_name)

    #add visit_type to covariates_of_curr_metavisit
    visit_types = vals[3].strip('{').strip('}').split(',')
    covariates_of_curr_metavisit.update(visit_types)

    #add start year of metavisit_period to covariates_of_curr_metavisit
    metavisit_period_start_year = vals[4].strip('[').strip('{').strip('(').split(',')[0][:4] #select year only
    if (metavisit_period_start_year != ''):
        covariates_of_curr_metavisit.add(metavisit_period_start_year)

    #add gender to covariates_of_curr_metavisit
    gender = vals[5].strip(' ')
    if (gender != ''):
        covariates_of_curr_metavisit.add(gender)

    #add age to covariates_of_curr_metavisit
    age = vals[6].strip(' ')
    if (age != ''):
        covariates_of_curr_metavisit.add(age)

    return covariates_of_curr_metavisit

##############################################################################
##########################PASS 2 Functions###################################
##############################################################################
def create_sparse_matrix_file(set_of_all_covariates):
    '''
    This function reads the output files created in pass 1 and generates CSR matrix.
    '''
    metavisit_labels = [] #store labels in a list
    metavisit_list = [] #store metavisits in a list
    inp_line_count = 0 #number of records
    #create a dictionary with covariate as key and its index as value.
    dict_of_covariates = {}
    for idx, cov in enumerate(set_of_all_covariates):
        dict_of_covariates[cov] = idx
    #print (dict_of_covariates, dict_of_covariates['46244959'], len(dict_of_covariates))

    # initialize parameters for CSR matrix
    rownums, colnums, total_row, total_col = initialize_csr_variables(set_of_all_covariates)
    #read records and generate sparse matrix
    print ('Generating CSR matrix....')
    with open(wv.all_metavisits_cov_file) as fin:
        for line in fin:
            inp_line_count+=1
            vals = line.rstrip().split(',')
            metavisit_list.append(vals[0])
            metavisit_labels.append(vals[1])
            covariates_of_curr_metavisit = vals[2:]
            #print (covariates_of_curr_metavisit)
            rownums, colnums = update_rownums_colnums_vectors(rownums, colnums, total_row, covariates_of_curr_metavisit, dict_of_covariates)
            total_row+=1
            if (inp_line_count%1000000 == 0):
                print ('{0} metavisits added to CSR matrix...'.format(inp_line_count))
            #if (total_row%7000000 == 0): #pickle.dump can save larger data. Still creating multiple files because scipy functions are too slow.
            #    save_compressed_sparse_matrix_to_file(rownums, colnums, total_row, total_col, step) #save CSR matrix as .dat file
            #    rownums, colnums, total_row, total_col = initialize_csr_variables(set_of_all_covariates)    #reset variables for the new file
            #    step+=1

    #DO NOT miss the the remaining records
    print ('{0} metavisits added to CSR matrix...'.format(inp_line_count))
    #save_compressed_sparse_matrix_to_file(rownums, colnums, total_row, total_col, step)
    save_compressed_sparse_matrix_to_file(rownums, colnums, total_row, total_col)
    return metavisit_list, metavisit_labels

def initialize_csr_variables(set_of_all_covariates):
    '''
    This function initializes the variables for CSR matrix
    '''
    rownums = []    # rownum vector for compresed sparse matrix
    colnums = []    # colnum vector for compresed sparse matrix
    total_row = 0  # number of metavisits in the input file
    total_col = len(set_of_all_covariates)   # number of covariates (features)
    return rownums, colnums, total_row, total_col

def update_rownums_colnums_vectors(rownums, colnums, total_row, covariates_of_curr_metavisit, dict_of_covariates):
    '''
    This function checks the index of covariates and adds that index to colnums vector
    for compresed sparse matrix.
    '''
    for cov in covariates_of_curr_metavisit:
        rownums.append(total_row)
        colnums.append(dict_of_covariates[cov])
    return rownums, colnums

#def save_compressed_sparse_matrix_to_file(rownums, colnums, total_row, total_col, step):
def save_compressed_sparse_matrix_to_file(rownums, colnums, total_row, total_col):
    '''
    This function uses rownums and colnums vectors to create compresed sparse
    matrix and then saves that matrix to a file.
    '''
    print ('Total rows: {0}, Total columns: {1}, Number of 1 in the sparse matrix: {2}, {3}'.format(total_row, total_col, len(rownums), len(colnums)))
    data = np.array([1]*len(rownums), dtype=np.int8)
    #X = sparse.csr_matrix((data, (rownums, colnums)), shape=(total_row, total_col), dtype=np.uint8)
    print ('Generating CSR matrix...........')
    X = sparse.csr_matrix((data, (rownums, colnums)), shape=(total_row, total_col))
    # save the files for future use as it takes hours to generate X
    print ('Saving the compresed sparse matrix on the disk....')
    #all_csr_file = csr_file + str(step) + '.dat'
    #all_csr_file = wv.all_metavisits_csr_file + '.dat'
    with open(wv.all_metavisits_csr_file, 'wb') as pf:
        pickle.dump(X, pf, pickle.HIGHEST_PROTOCOL)

def save_metavisits_to_file(metavisit_list):
    '''
    This function saves all the metavisits present in the list to a file. Although the
    metavisits are present in a file with their covariates, but loading data from
    that file takes time.
    '''
    fmv = open(wv.all_metavisits_file, 'w')
    line = ','.join(metavisit for metavisit in metavisit_list)
    fmv.write(line)
    fmv.close()

def save_labels_to_file(metavisit_labels):
    '''
    This function saves all the labels present in the list to a file. Although the
    labels are present in a file with metavisits, but loading data from
    that file takes time.
    '''
    fl = open(wv.all_labels_file, 'w')
    line = ','.join(label for label in metavisit_labels)
    fl.write(line)
    fl.close()


def main():
    '''
    This is the main function to call all other functions.
    '''
    ##############################################PHASE 1#######################################
    ## 1. Deteremine all covariates using all metavisits present in the input file.
    ## 2. Create an output file with metavisits, their labels and all covariates as records.
    ###########################################################################################
    print ('Performing operations in phase 1....')
    print ('\nCreating concept_code_concept_id dictionary and finding concept ids....')
    stime = time.time()
    concept_code_id_dict, icd9_icd10cm_concepts_set, SNOMED_concepts_set, CPT4_concepts_set, ICD9Proc_concepts_set = find_concept_ids_of_concept_codes()
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    print ('\nFinding the mapping between CPT4/ICD9Proc and ICD10Proc....')
    stime = time.time()
    icd9_icd10_proc_dict, cpt4_icd10_proc_dict = find_icd10proc_for_icd9proc_and_cpt4_concept_ids(concept_code_id_dict)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    print ('\nFinding ancestors for all descendant concpet ids...')
    stime = time.time()
    descendant_ancestor_dict = find_ancestors_of_concepts()
    print ('Number of keys in descendant_ancestor_dict dictionary: {0}'.format(len(descendant_ancestor_dict.keys())))
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))
    #print (concept_code_id_dict['00100'], concept_code_id_dict['0C5J3ZZ'])
    #print (len(icd9_icd10_proc_dict['2002191']), icd9_icd10_proc_dict['2002191'])
    #print (len(cpt4_icd10_proc_dict['2616463']), cpt4_icd10_proc_dict['2616463'])
    #print (descendant_ancestor_dict['242'])

    print ('\nFinding covariates and labels for all metavisits...')
    stime = time.time()
    set_of_all_covariates = find_covariates_and_label_for_metavisit(icd9_icd10cm_concepts_set, CPT4_concepts_set, ICD9Proc_concepts_set, SNOMED_concepts_set,
                                                                    descendant_ancestor_dict, icd9_icd10_proc_dict, cpt4_icd10_proc_dict)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    #Save all covariates in a file
    print ('\nSaving all covariates in a file...')
    stime = time.time()
    fc = open(wv.all_covfile, 'w')
    set_of_all_covariates = list(set_of_all_covariates)  #convert set to list as set does not maintain sequence
    line = ','.join(cov for cov in set_of_all_covariates)
    fc.write(line)
    fc.close()
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))
    print ('Phase 1 finished successfully....')

    ##############################################PHASE 2########################################
    ## Create compresed sparse matrix file using the output files created in PHASE 1
    ############################################################################################
    print ('\nPerforming operations in phase 2....')
    ##########################################################################################
    #The following commented code is needed only if program terminated/failed after running Phase 1.
    #with open(wv.all_covfile) as fcov:
    #    for line in fcov:
    #        set_of_all_covariates = line.split(',')
    #print ('Total number of covariates: {0}'.format(len(set_of_all_covariates)))
    #########################################################################################
    print ('Create CSR matrices and save them to files...')
    stime = time.time()
    metavisit_list, metavisit_labels = create_sparse_matrix_file(set_of_all_covariates)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    #Save all metavisits in a file
    print ('\nSaving all metavisits to a file...')
    stime = time.time()
    save_metavisits_to_file(metavisit_list)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    #Save all labels in a file
    print ('\nSaving all labels to a file...')
    stime = time.time()
    save_labels_to_file(metavisit_labels)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    print ('Phase 2 finished successfully....')
    print ('\nTHE END: the program finished successfully')


############# INPUT and OUTPUT FILES ###########################################
#inpfile = 'SQL/output/ccaemdcr2003_16_er_inpatient_suicidality_metavisits.tsv.gz'
#metavisits_cov_file = 'main_outputs/all_metavisits_2003_16_with_covariates.txt'
#metavisits_file = 'train_files/full_data/all_metavisits_2003_16.txt'
#labels_file = 'train_files/full_data/all_labels_2003_16.txt'
#covfile = 'train_files/all_covariates_2003_16.txt'
#csr_file = 'train_files/full_data/all_metavisits_csr_data_2003_16'
############# INPUT and OUTPUT FILES ###########################################

if __name__ == '__main__':
    '''
    This program reads the output file created by SQL and generates various output
    files for machine language codes.
    This program has been divided into two phases:
    Phase 1: Using all metavisits present in the output file created by SQL, it
             generates a set of covariates (concept_ids, concept_set_name,
             visit_type, age, gender). All ICD9Proc and CPT4 concepts are converted
             into their equivalent ICD10PCS concepts. ICD9CM and ICD10CM concepts
             are excluded from the set. Thus, the set of covariates comprises ICD10PCS
             concepts and their ancestors, SNOMED concepts and their ancestors, OMOP concepts,
             user-defined concept_set_name, and visit_types (inpatient/outpatient
             /emergency), age and gender.
             It also creates a big metavisit file wherein each row contains metavisit_id,
             label for the metavisit_id (1/0), and list of covariates for the
             metavisit_id. Label is set to 1 if the metavisit has at least one of the
             codes identified for suicide/self_harm, otherwise, 0.

    Phase 2: Using the big metavisit file and covariate file created in phase 1,
             it creates compressed sparse matrix, list of metavisits, and list of
             labels for each of those metavisits. CSR matrix is saved as .dat file
             using picke library. metavisit list and label list files are also
             saved. As the execution of this program takes about 20-24 hours, all
             these files are saved on disk for the classifier programs.
    '''
    main()
