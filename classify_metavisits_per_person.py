import workvariables as wv
import time
import numpy as np
import pickle
from scipy import sparse
import os
import gzip
import xgboost as xgb
import random
import collections
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import average_precision_score
from datetime import datetime

def generate_person_mv_dictionary():
    '''
    This function generates a dictionary with person_id as key and metavisits as
    values.
    '''
    print ('\nGenerate person metavisit dictionary............>>>')
    line_count = 0
    person_mv_dict = {}
    with gzip.open(wv.mv_person_file, 'rb') as fin:
        for line in fin:
            line_count+=1
            if (line_count==1): #skip header
                continue
            if (line_count%5000000 == 0):
                print ('Records processed............{0}'.format(line_count))
            vals = line.decode('utf8').rstrip().split('\t')
            if (vals[1] not in person_mv_dict):
                person_mv_dict[vals[1]] = [vals[0].strip(' ')]
            else:
                person_mv_dict[vals[1]].append(vals[0].strip(' '))
    print ('Records processed............{0}'.format(line_count))
    return person_mv_dict

def create_train_test_set(person_mv_dict):
    '''
    One person can have multiple metavisits. This function selects one metavisit
    per person and includes that metavisit into train set. If a person has more than
    one metavisits, the other metavisits are added to test set.
    '''
    print ('\nSelect one metavisit per person for the classifier...........>>>')
    train_mv = []
    test_mv = []
    for person_id, mv_id in person_mv_dict.items():
        idx = np.random.permutation(len(mv_id)) #radnomly select one metavisit per person for train set
        for i in range(len(mv_id)):
            if (i==0):
                train_mv.append(mv_id[idx[i]])
            else:
                test_mv.append(mv_id[idx[i]])
    return np.asarray(train_mv), np.asarray(test_mv)

def load_csr_file_records():
    '''
    This function reads the CSR matrix from the saved file.
    '''
    print ('\nFetch data, labels, and metavisits from CSR file........>>>')
    print ('Loading .dat file into RAM....')
    with open(wv.all_metavisits_csr_file, 'rb') as pif:
        data1 = pickle.load(pif)
        print (data1.__getattr__)
    return data1, np.asarray(get_metavisit_labels(data1.shape[0]), dtype=np.int8), np.asarray(fetch_all_metavisits(data1.shape[0]))

def fetch_all_metavisits(no_of_records):
    '''
    This function reads the all_metavisits files and stores metavisits in a list.
    '''
    print ('Fetching all metavisits from file and storing them in a list... ')
    with open(wv.all_metavisits_file) as fmv:
        for line in fmv:
            metavisits = line.rstrip().split(',')
    print ('Number of metavisits in the file: {0}'.format(len(metavisits)))
    return (metavisits[:no_of_records])

def get_metavisit_labels(no_of_records):
    '''
    This function reads the metavisit_label file returns the labels.
    '''
    print ('Fetching labels for the metavisits...')
    with open(wv.all_labels_file) as fl:
        for line in fl:
            labels = line.rstrip().split(',')
    print ('Number of labels in the file: {0}'.format(len(labels)))
    return (labels[:no_of_records])

def split_train_validation_set(X_all, Y_all, metavisits_all):
    '''
    This function splits the data into train and validation sets. The records are
    selected randomly. 30% records represent validation set and 70% records represent
    train set.
    '''
    print ('\nCreate train and validation sets.....>')
    random_idx = [i for i in range(len(Y_all))]
    random.shuffle(random_idx)
    train_idx = random_idx[:int(len(Y_all)*0.7)]
    validation_idx = random_idx[int(len(Y_all)*0.7):]
    return X_all[train_idx], Y_all[train_idx], metavisits_all[train_idx], X_all[validation_idx], Y_all[validation_idx], metavisits_all[validation_idx]

def evalmcc(preds, dtrain):
    THRESHOLD = 0.5
    labels = dtrain.get_label()
    return 'MCC', matthews_corrcoef(labels, preds >= THRESHOLD)

def classify_the_train_data (X, Y, metavisits_list):
    '''
    This function calls xgboost classifier to classify the data and compute
    several performance metrices.
    '''
    imp_features = {}
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    # compute indices for train and test data for 5-fold validation
    print ('Generating train and test indices for 5-fold validation....')
    v = np.random.permutation(X.shape[0])
    r5 = len(v) % 5
    i5 = 0
    for i in range(5):
        lv5 = int(len(v)/5)
        if(r5 > 0):
            lv5 = lv5+1
            r5 = r5-1
        teidx = v[i5:min(i5+lv5,len(v))]
        tridx = np.append(v[0:i5], v[min(i5+lv5,len(v)):len(v)])
        i5 = i5 + lv5

        print ('Performing fold number {0}'.format(i+1))
        print ('Train the classifier....')
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric=evalmcc, verbose=False)
        bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx])
        #bst = xgb.XGBClassifier().fit(X[tridx], Y[tridx])

        print ('Test the classifier....')
        preds = bst.predict_proba(X[teidx])  # this will return probabilities

        print ('Finding important features.............')
        imps = bst.get_booster().get_score(importance_type='gain')
        imp_features.update(imps)   # update dictionary i.e. if key is alreay present, update the value, else add new key

        print ('Storing the computed propabilities in a dictionary....')
        y_test = Y[teidx]
        for j in range(len(teidx)):
            metavisit_suicide_prob[metavisits_list[teidx[j]]] = (preds[j], y_test[j])

    return metavisit_suicide_prob, imp_features

def classify_test_validation_data (X_tr, Y_tr, X_te, Y_te, metavisits_te):
    '''
    This function trains the classifier using train data and test the trained
    classifier using test data.
    '''
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y_tr)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    print ('Train the classifier....')
    #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric=evalmcc, verbose=False)
    bst = xgb.XGBClassifier(**param).fit(X_tr, Y_tr)
    print ('Test the classifier....')
    preds = bst.predict_proba(X_te)  # this will return probabilities
    print ('Storing the computed propabilities in a dictionary....')
    for j in range(len(Y_te)):
        metavisit_suicide_prob[metavisits_te[j]] = (preds[j], Y_te[j])
    return metavisit_suicide_prob


def get_accuracy_auc_mcc_score(metavisit_suicide_prob):
    '''
    This function computes the accuracy, AUC, and MCC of the classifier.
    '''
    y_true = []
    y_pred_acc = []
    y_pred_auc = []
    for mv, pred in metavisit_suicide_prob.items():
        y_true.append(pred[1])
        y_pred_auc.append(pred[0][1])
        y_pred_acc.append(round(pred[0][1]))
    # evaluate predictions
    print('Accuracy of the classifier is...............: {0}'.format(accuracy_score(y_true, y_pred_acc)))
    print('MCC of the classifier is....................: {0}'.format(matthews_corrcoef(y_true, y_pred_acc)))
    print('AUC for the classifier is...................: {0}'.format(roc_auc_score(y_true, y_pred_auc)))
    print('Avg precision-recall for the classifier is..: {0}'.format(average_precision_score(y_true, y_pred_auc)))


def find_important_features(imp_features, iff):
    '''
    This function finds the features that were used by the classifier.
    '''
    # get all features/covariates in a list
    with open(wv.all_covfile) as cfl:
        for line in cfl:
            covars = line.strip().split(',')

    #get the concept_name for each concept_id
    print ('Creating concept_id_name dictionary with concept_id as key and concept_name as value....')
    lcount=-1
    c_id = 0
    c_name = 0
    concept_id_name_dict = {}
    with open(wv.concept_file) as cf:
        for line in cf:
            lcount+=1
            vals = line.strip().split('\t')
            if(lcount==0):
                for idx, col in enumerate(vals):
                    if (col == 'concept_id'):
                        c_id = idx
                    elif(col == 'concept_name'):
                        c_name = idx
            else:
                concept_id_name_dict[vals[c_id]] = vals[c_name]

    print ('Writing the important features to the file............')
    header_line = 'Concept_id ' + '\t' + 'Concept_name' + '\t' + 'Gain Score' + '\n\n'
    iff.write(header_line)
    ###START - Some housekeeping to avoid key-error.
    int_test = set('abcdefghijklmnopqrstuvwxyz')    #for user-defined concepts, there is no concept_name. Concept_ids are integers only.
    non_concepts = set(['2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016']) #satrt year of metavisit_period
    for i in range(100): #append age
        non_concepts.add(str(i))
    ###END - Some housekeeping to avoid key-error.
    for k in sorted(imp_features, key=imp_features.get, reverse=True): # sort the dictionary in decreasing order of value
        pos = int(k[1:])
        if (len(int_test.intersection(covars[pos])) > 0 or covars[pos] in non_concepts):
            line = covars[pos] + '\t' + '-' + '\t' + str(imp_features[k]) + '\n'
        else:
            line = covars[pos] + '\t' + concept_id_name_dict[covars[pos]] + '\t' + str(imp_features[k]) + '\n'
        iff.write(line)
    iff.close()

def save_classification_results_in_a_file(metavisit_suicide_self_harm_prob, mpf):
    '''
    This function writes metavisits and their corresponding true class and predicted
    probabilities for class 0 and class 1 to a file.
    '''
    # write metavisit and its probabilty to an output file
    for mvisit, pred in metavisit_suicide_self_harm_prob.items():
        line = str(mvisit) + ' : ' + str(pred[0]) +  ' : ' + str(pred[1]) + '\n'
        mpf.write(line)


def main():
    '''
    Start of the code. PER-PERSON-MODEL
    '''
    #create a dictionary with person_id as key and metavisits as value
    person_mv_dict = generate_person_mv_dictionary()
    print ('Number of unique person in the set....{0}'.format(len(person_mv_dict)))
    #select one mv person as train mv
    train_mv, test_mv = create_train_test_set(person_mv_dict)
    print (len(train_mv), len(test_mv))

    # load CSR data from .dat file
    X_all, Y_all, metavisits_all = load_csr_file_records()
    print ('\nSelect indices of train/test metavisits and generate data for classifier....>>>')
    tr_idx = np.isin(metavisits_all, train_mv).nonzero()[0] #train indices
    te_idx = np.isin(metavisits_all, test_mv).nonzero()[0] #test indices
    print (len(tr_idx),len(te_idx))

    #train data
    X_tr = X_all[tr_idx]
    Y_tr = Y_all[tr_idx]
    metavisits_tr = metavisits_all[tr_idx]
    print (X_tr.__getattr__)
    print ('Number of metavisit with label 1 in train set is.... {0}'.format(np.sum(Y_tr)))
    #Using dataset with one mv/person, create train and validation set
    X_train, Y_train, metavisits_train, X_val, Y_val, metavisits_val = split_train_validation_set(X_tr, Y_tr, metavisits_tr)
    print ('Number of 1s in the validation set: {0}, number of 1s in test set:{1}'.format(np.sum(Y_val), np.sum(Y_train)))

    ##### Classify the data ###############
    print ('\nClassify the train metavisits (one mv per person) using 5 fold CV.............>>>')
    metavisit_suicide_prob, imp_features = classify_the_train_data (X_tr, Y_tr, metavisits_tr)
    #Determine AUC, Accuracy and important features.
    print ('\nDetermine the performance of the classifier with train set........> ')
    get_accuracy_auc_mcc_score(metavisit_suicide_prob)

    #save classification results in a file
    print ('\nSave classification results using train set in a file...........>')
    mpf = open(wv.pp_mv_probs_file, 'w')   #file to store metavisits and their corresponding probabilties
    save_classification_results_in_a_file(metavisit_suicide_prob, mpf)
    #save the important features
    print ('\nCreate a file with important features................> ')
    iff = open(wv.pp_imp_covar_file, 'w') # file to store important features
    find_important_features(imp_features, iff)

    #test the validation data (30%) on the model trained with 70% of mv
    print ('\nClassify the validation metavisits (30% of one mv per person).............> ')
    metavisit_suicide_prob = classify_test_validation_data (X_train, Y_train, X_val, Y_val, metavisits_val)
    #Determine AUC, Accuracy for train data (5-fold)
    print ('\nDetermine the performance metrics of the classifier with validation set........> ')
    get_accuracy_auc_mcc_score(metavisit_suicide_prob)

    #test data - rest of the metavisits
    X_te = X_all[te_idx]
    Y_te = Y_all[te_idx]
    metavisits_te = metavisits_all[te_idx]
    print (X_te.__getattr__)
    print ('Number of metavisit with label 1 in test set is.... {0}'.format(np.sum(Y_te)))

    #test the trained classifier using test data.
    print ('\nTest the classifier using test data (extra mv per person).........>>>>')
    metavisit_suicide_prob = classify_test_validation_data (X_tr, Y_tr, X_te, Y_te, metavisits_te)
    #Determine AUC, Accuracy and important features.
    print ('\nDetermine the performance of the classifier using test data........> ')
    get_accuracy_auc_mcc_score(metavisit_suicide_prob)

    print ('\nProgram finished successfully!!!')

################################################################################
###                            Input Output files                            ###
################################################################################
#mv_person_file = 'SQL/output/ccae2003_16_er_inpatient_suicidality_metavisit_period_data.tsv.gz'
#csr_file = 'train_files/all_metavisits/all_metavisits_csr_data_2003_16.dat'
#metavisits_file = 'train_files/all_metavisits/all_metavisits_2003_16.txt'
#labels_file = 'train_files/all_metavisits/all_labels_2003_16.txt'
#csr_file = 'train_files/balanced_data/bal_metavisits_csr_data_2003_16.dat'
#metavisits_file = 'train_files/balanced_data/bal_metavisits_2003_16.txt'
#labels_file = 'train_files/balanced_data/bal_labels_2003_16.txt'
#covfile = 'train_files/all_covariates_2003_16.txt'
#imp_covar_file = 'classifier_outputs/per_person_2003_16_important_covariates_'
#mv_probs_file = 'classifier_outputs/per_person_2003_16_metavisits_probs_'

if __name__ == '__main__':
    '''
    This program selects one meta-visit per person as train set and rest of the meta-visits
    as test set. XGboost is first trained and tested on the dataset comprising one meta-visit
    per person using 5-fold cv. Model trained using train set is also tested on the test set.
    '''
    main()
