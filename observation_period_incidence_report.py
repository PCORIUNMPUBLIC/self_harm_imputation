from datetime import datetime
import csv
import gzip
import argparse

def find_person_id_for_classified_metavisits():
    '''
    Create a set of person_id whose metavisits were classified by the classifier.
    Selecting person_id will make sure that observations for the metavisits that
    were not classified are not counted.
    '''
    all_person_id = set()
    line_count = -1
    #get the list of all metavisits
    with open(all_metavisits) as allmv:
        for line in allmv:
            t = line
    mvs = set(t.strip().split(','))
    skip_count = 0

    print ('\nCreate a set of person_id for the classified metavisits.....>>>')
    with gzip.open(mv_demography_file, 'rb') as mdf:
        for line in mdf:
            line_count+=1
            if (line_count%2500000==0):
                print('Number of records processed.................> {0}'.format(line_count))
            vals = line.decode('utf8').strip().split('\t') #for zipped file record
            if (line_count <=0): #fist line is header, get the index of columns
                for i,v in enumerate(vals):
                    if (v.strip(' ') == 'metavisit_occurrence_id'):
                        mv_idx = i
                    elif (v.strip(' ') == 'person_id'):
                        p_idx = i
                    elif (v.strip(' ') == 'metavisit_period'):
                        mvp_idx = i
                    elif (v.strip(' ') == 'yob'):
                        yob_idx = i
                    elif (v.strip(' ') == 'state'):
                        state_idx = i
                    elif (v.strip(' ') == 'gender'):
                        gender_idx = i
                    else:
                        print('Unknown column')
            else: #actual record
                mv = vals[mv_idx].strip(' ')
                pid = vals[p_idx].strip(' ')
                if (mv in mvs):
                    all_person_id.add(pid)
                else:
                    skip_count+=1
    print ('Total person_id...{0}, Total metavisits...{1}, skip_count...{2}'.format(len(all_person_id), line_count, skip_count))
    return all_person_id

def select_metavisits_probs():
    '''
    This function reads the classification output file created by the classifier
    and creates 2 dictionaries. One dictionary for coded metavisits with their
    probabilties, another dictionary for the remaining metavisits with their
    probabilties.
    '''
    print ('\nSelect coded and imputed metavisits.....>>>')
    coded_imputed_mv = {} # only coded
    uncoded_imputed_mv = {} #uncoded
    line_count = -1
    with open(mv_prob_file, 'r') as mpf:
        for line in mpf:
            line_count+=1
            if(line_count%2500000 == 0):
                print ('{0} records processed...................>'.format(line_count))
            vals = line.strip().split(':')
            mv = vals[0].strip(' ')
            prob_0 = float(vals[1].strip().strip('[').strip(']').split(' ')[0]) #probabilty for class 0
            prob_1 = 1-prob_0  #doing this stupid thing because of spaces in the string.
            tru_class = vals[2].strip(' ')
            if (tru_class == '1'):
                coded_imputed_mv[mv] = prob_1
            else:
                uncoded_imputed_mv[mv] = prob_1
    print ('Records processed...................> {0}, {1}'.format(line_count, len(coded_imputed_mv)+len(uncoded_imputed_mv)))
    print ('Coded...{0}, Uncoded...{1}'.format(len(coded_imputed_mv), len(uncoded_imputed_mv)))
    print ('Total imputed mv....{0}'.format(sum(coded_imputed_mv.values()) + sum(uncoded_imputed_mv.values())))
    return coded_imputed_mv, uncoded_imputed_mv

def get_imputed_metavisits_probs_by_age(coded_imputed_mv, uncoded_imputed_mv, span):
    '''
    This function creates 3 dictionaries:
    One dictionary contains probabilties for coded metavisits by age.
    Second dictionary contains probabilties for non-coded metavisits by age.
    Third dictionary contains counts for coded metavisits by age.
    '''
    print ('\nCreate dictionaries for metavisits each year by age........>>>')
    coded_imputed_mv_probs_by_age = {} # probs for coded
    coded_imputed_mv_count_by_age = {} # count for coded
    uncoded_imputed_mv_probs_by_age = {} #probs for non-coded
    line_count = -1
    skip_count = 0

    with gzip.open(mv_demography_file, 'rb') as mdf:
        for line in mdf:
            line_count+=1
            if (line_count%2500000==0):
                print('Number of records processed.................> {0}'.format(line_count))
            vals = line.decode('utf8').strip().split('\t') #for zipped file record
            if (line_count <=0): #fist line is header, get the index of columns
                for i,v in enumerate(vals):
                    if (v.strip(' ') == 'metavisit_occurrence_id'):
                        mv_idx = i
                    elif (v.strip(' ') == 'person_id'):
                        p_idx = i
                    elif (v.strip(' ') == 'metavisit_period'):
                        mvp_idx = i
                    elif (v.strip(' ') == 'yob'):
                        yob_idx = i
                    elif (v.strip(' ') == 'state'):
                        state_idx = i
                    elif (v.strip(' ') == 'gender'):
                        gender_idx = i
                    else:
                        print('Unknown column')
            else: #actual record
                mv = vals[mv_idx].strip(' ')
                yob = int(vals[yob_idx].strip(' '))
                date_pair = vals[mvp_idx].strip('[').strip(')').split(',')
                st_year = datetime.strptime(date_pair[0].strip(' '), '%Y-%m-%d')
                end_year = datetime.strptime(date_pair[1].strip(' '), '%Y-%m-%d')

                #coded metavisit
                if (mv in coded_imputed_mv):
                    for i in range(st_year.year, end_year.year+1, 1):
                        if (i-yob in coded_imputed_mv_probs_by_age): #if age already there in dictionary
                            coded_imputed_mv_probs_by_age[i-yob][i-DATA_START_YEAR]+=coded_imputed_mv[mv] #add prob_1
                            coded_imputed_mv_count_by_age[i-yob][i-DATA_START_YEAR]+=1
                        else:   #if age not in dictionary
                            coded_imputed_mv_probs_by_age[i-yob]= [0]*span
                            coded_imputed_mv_probs_by_age[i-yob][i-DATA_START_YEAR]+=coded_imputed_mv[mv] #add prob_1
                            coded_imputed_mv_count_by_age[i-yob]= [0]*span
                            coded_imputed_mv_count_by_age[i-yob][i-DATA_START_YEAR]+=1
                #non coded probables
                elif (mv in uncoded_imputed_mv):
                    for i in range(st_year.year, end_year.year+1, 1):
                        if (i-yob in uncoded_imputed_mv_probs_by_age): #if age already there in dictionary
                            uncoded_imputed_mv_probs_by_age[i-yob][i-DATA_START_YEAR]+=uncoded_imputed_mv[mv] #add prob_1
                        else: #if age not in dictionary
                            uncoded_imputed_mv_probs_by_age[i-yob] = [0]*span
                            uncoded_imputed_mv_probs_by_age[i-yob][i-DATA_START_YEAR]+=uncoded_imputed_mv[mv] #add prob_1
                else:
                    print ('Metavisit was not present in classifier output...{0}'.format(mv))
                    skip_count+=1

    print('Total records...{0}, Skipped records...{1}, Number of records processed...{2}'.format(line_count, skip_count, line_count-skip_count))
    return coded_imputed_mv_probs_by_age, uncoded_imputed_mv_probs_by_age, coded_imputed_mv_count_by_age


def get_observation_count_by_age(span, all_person_id):
    '''
    Denominator: Find the observation count in each year. If a person is observed for only
    x days in a year, add x/365 or x/366 to the count.
    '''
    print ('\nCount the observations each year by age..........>>>>')
    line_count = -1
    skip_count = 0
    observation_count_by_age = {}
    print ('Find number of persons observed each year....')
    with gzip.open(person_observation_file, 'rb') as pof:
        for line in pof:
            line_count+=1
            if (line_count%2000000 == 0):
                print('Observations records processed.....................> {0}'.format(line_count))
            #if (line_count == 1000000): ##for TESTING only
            #    break
            vals = line.decode('utf8').strip().split('\t') #for zipped file record
            if (line_count <=0): #fist line is header, get the index of columns
                for i,v in enumerate(vals):
                    if (v.strip(' ') == 'person_id'):
                        p_idx = i
                    elif (v.strip(' ') == 'observation_period_start_date'):
                        obs_idx = i
                    elif (v.strip(' ') == 'observation_period_end_date'):
                        obe_idx = i
                    elif (v.strip(' ') == 'state'):
                        state_idx = i
                    elif (v.strip(' ') == 'yob'):
                        yob_idx = i
                    else:
                        print('Unknown column')
            else: #actual record
                pid = vals[p_idx].strip(' ')
                if (pid not in all_person_id):
                    skip_count+=1
                    continue
                yob = int(vals[yob_idx].strip(' '))
                #### START - Fractional counting for a year depending on number of days observed.
                # e.g. if observation started in 2003 and ended in 2007, add 1 to observation_count_by_age for 2004, 2005, and 2006
                ob_start = datetime.strptime(vals[obs_idx].strip(' '), '%Y-%m-%d')
                ob_end = datetime.strptime(vals[obe_idx].strip(' '), '%Y-%m-%d')
                if (ob_start.year == ob_end.year):
                    if (ob_start.year==2004 or ob_start.year==2008 or ob_start.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((ob_end - ob_start).days + 1)/n_days
                    if (ob_start.year-yob in observation_count_by_age): #if age already in dictionary
                        observation_count_by_age[ob_start.year-yob][ob_start.year-DATA_START_YEAR]+=ob_days
                    else: #if age not in dictionary
                        observation_count_by_age[ob_start.year-yob] = [0]*span
                        observation_count_by_age[ob_start.year-yob][ob_start.year-DATA_START_YEAR]+=ob_days
                else:
                    for i in range(ob_start.year+1, ob_end.year, 1):
                        if (i-yob in observation_count_by_age):
                            observation_count_by_age[i-yob][i-DATA_START_YEAR]+=1
                        else:
                            observation_count_by_age[i-yob] = [0]*span
                            observation_count_by_age[i-yob][i-DATA_START_YEAR]+=1

                    #special processing for start date
                    e1 = datetime.strptime(str(ob_start.year)+'-12-31','%Y-%m-%d') #end of the year
                    if (ob_start.year==2004 or ob_start.year==2008 or ob_start.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((e1 - ob_start).days + 1)/n_days
                    if (ob_start.year-yob in observation_count_by_age): #if age already in dictionary
                        observation_count_by_age[ob_start.year-yob][ob_start.year-DATA_START_YEAR]+=ob_days
                    else: #if age not in dictionary
                        observation_count_by_age[ob_start.year-yob] = [0]*span
                        observation_count_by_age[ob_start.year-yob][ob_start.year-DATA_START_YEAR]+=ob_days

                    #special processing for end date
                    e2 = datetime.strptime(str(ob_end.year)+'-01-01','%Y-%m-%d')
                    if (ob_end.year==2004 or ob_end.year==2008 or ob_end.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((ob_end - e2).days + 1)/n_days
                    if (ob_end.year-yob in observation_count_by_age): #if age already in dictionary
                        observation_count_by_age[ob_end.year-yob][ob_end.year-DATA_START_YEAR]+=ob_days
                    else: #if age not in dictionary
                        observation_count_by_age[ob_end.year-yob] = [0]*span
                        observation_count_by_age[ob_end.year-yob][ob_end.year-DATA_START_YEAR]+=ob_days
    print('Observations records processed....................> {0}'.format(line_count))
    print('Observations records skipped......................> {0}'.format(skip_count))
    return observation_count_by_age


def get_imputed_metavisits_probs_by_state (coded_imputed_mv, uncoded_imputed_mv, span):
    '''
    This function creates 3 dictionaries:
    One dictionary contains probabilties for coded metavisits by state.
    Second dictionary contains probabilties for non-coded metavisits by state.
    Third dictionary contains counts for coded metavisits by state.
    '''
    print ('\nCounts of different types of metavisits each year by state........>>>')
    coded_imputed_mv_probs_by_state = {} # probs for coded
    coded_imputed_mv_count_by_state = {} # count for coded
    uncoded_imputed_mv_probs_by_state = {} #probs for non-coded
    line_count = -1
    skip_count = 0

    ##start processing
    with gzip.open(mv_demography_file, 'rb') as mdf:
        for line in mdf:
            line_count+=1
            if (line_count%2500000==0):
                print('Number of records processed.................> {0}'.format(line_count))
            #vals = line.strip().split('\t')
            vals = line.decode('utf8').strip().split('\t') #for zipped file record
            if (line_count <=0): #fist line is header, get the index of columns
                for i,v in enumerate(vals):
                    if (v.strip(' ') == 'metavisit_occurrence_id'):
                        mv_idx = i
                    elif (v.strip(' ') == 'person_id'):
                        p_idx = i
                    elif (v.strip(' ') == 'metavisit_period'):
                        mvp_idx = i
                    elif (v.strip(' ') == 'yob'):
                        yob_idx = i
                    elif (v.strip(' ') == 'state'):
                        state_idx = i
                    elif (v.strip(' ') == 'gender'):
                        gender_idx = i
                    else:
                        print('Unknown column')
            else: #actual record
                mv = vals[mv_idx].strip(' ')
                state = vals[state_idx].strip(' ')
                if (state not in US_STATES):
                    state = 'OT' #other territories
                date_pair = vals[mvp_idx].strip('[').strip(')').split(',')
                st_year = datetime.strptime(date_pair[0].strip(' '), '%Y-%m-%d')
                end_year = datetime.strptime(date_pair[1].strip(' '), '%Y-%m-%d')
                #coded metavisit
                if (mv in coded_imputed_mv):
                    for i in range(st_year.year, end_year.year+1, 1):
                        if (state in coded_imputed_mv_probs_by_state): #if state already there in dictionary
                            coded_imputed_mv_probs_by_state[state][i-DATA_START_YEAR]+=coded_imputed_mv[mv] #add prob_1
                            coded_imputed_mv_count_by_state[state][i-DATA_START_YEAR]+=1
                        else:   #if state not in dictionary
                            coded_imputed_mv_probs_by_state[state]= [0]*span
                            coded_imputed_mv_probs_by_state[state][i-DATA_START_YEAR]+=coded_imputed_mv[mv] #add prob_1
                            coded_imputed_mv_count_by_state[state]= [0]*span
                            coded_imputed_mv_count_by_state[state][i-DATA_START_YEAR]+=1
                #non coded probables
                elif (mv in uncoded_imputed_mv):
                    for i in range(st_year.year, end_year.year+1, 1):
                        if (state in uncoded_imputed_mv_probs_by_state): #if state already there in dictionary
                            uncoded_imputed_mv_probs_by_state[state][i-DATA_START_YEAR]+=uncoded_imputed_mv[mv] #add prob_1
                        else: #if state not in dictionary
                            uncoded_imputed_mv_probs_by_state[state] = [0]*span
                            uncoded_imputed_mv_probs_by_state[state][i-DATA_START_YEAR]+=uncoded_imputed_mv[mv] #add prob_1
                else:
                    print ('Metavisit was not present in classifier output...{0}'.format(mv))
                    skip_count+=1

    print('Total records...{0}, Skipped records...{1}, Number of records processed...{2}'.format(line_count, skip_count, line_count-skip_count))
    return coded_imputed_mv_probs_by_state, uncoded_imputed_mv_probs_by_state, coded_imputed_mv_count_by_state


def get_observation_count_by_state(span, all_person_id):
    '''
    Denominator: Find the observation count in each year. If a person is observed for only
    x days in a year, add x/365 or x/366 to the count.
    '''
    print ('\nCount the observations each year by state..........>>>>')
    line_count = -1
    skip_count = 0
    observation_count_by_state = {}
    print ('Find number of persons observed each year....')
    with gzip.open(person_observation_file, 'rb') as pof:
        for line in pof:
            line_count+=1
            if (line_count%2000000 == 0):
                print('Observation records processed.....................> {0}'.format(line_count))
            vals = line.decode('utf8').strip().split('\t') #for zipped file record
            if (line_count <=0): #fist line is header, get the index of columns
                for i,v in enumerate(vals):
                    if (v.strip(' ') == 'person_id'):
                        p_idx = i
                    elif (v.strip(' ') == 'observation_period_start_date'):
                        obs_idx = i
                    elif (v.strip(' ') == 'observation_period_end_date'):
                        obe_idx = i
                    elif (v.strip(' ') == 'state'):
                        state_idx = i
                    elif (v.strip(' ') == 'yob'):
                        yob_idx = i
                    else:
                        print('Unknown column')
            else: #actual record
                #### START - Fractional counting for a year depending on number of days observed.
                # e.g. if observation started in 2003 and ended in 2007, add 1 to observation_count_by_state for 2004, 2005, and 2006
                state = vals[state_idx].strip(' ')
                pid = vals[p_idx].strip(' ')
                if (pid not in all_person_id):
                    skip_count+=1
                    continue
                if (state not in US_STATES):
                    state = 'OT' #other territories
                ob_start = datetime.strptime(vals[obs_idx].strip(' '), '%Y-%m-%d')
                ob_end = datetime.strptime(vals[obe_idx].strip(' '), '%Y-%m-%d')
                if (ob_start.year == ob_end.year):
                    if (ob_start.year==2004 or ob_start.year==2008 or ob_start.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((ob_end - ob_start).days + 1)/n_days
                    if (state in observation_count_by_state): #if state already in dictionary
                        observation_count_by_state[state][ob_start.year-DATA_START_YEAR]+=ob_days
                    else: #if age not in dictionary
                        observation_count_by_state[state] = [0]*span
                        observation_count_by_state[state][ob_start.year-DATA_START_YEAR]+=ob_days
                else:
                    for i in range(ob_start.year+1, ob_end.year, 1):
                        if (state in observation_count_by_state):
                            observation_count_by_state[state][i-DATA_START_YEAR]+=1
                        else:
                            observation_count_by_state[state] = [0]*span
                            observation_count_by_state[state][i-DATA_START_YEAR]+=1

                    #special processing for start date
                    e1 = datetime.strptime(str(ob_start.year)+'-12-31','%Y-%m-%d') #end of the year
                    if (ob_start.year==2004 or ob_start.year==2008 or ob_start.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((e1 - ob_start).days + 1)/n_days
                    if (state in observation_count_by_state): #if age already in dictionary
                        observation_count_by_state[state][ob_start.year-DATA_START_YEAR]+=ob_days
                    else: #if age not in dictionary
                        observation_count_by_state[state] = [0]*span
                        observation_count_by_state[state][ob_start.year-DATA_START_YEAR]+=ob_days

                    #special processing for end date
                    e2 = datetime.strptime(str(ob_end.year)+'-01-01','%Y-%m-%d')
                    if (ob_end.year==2004 or ob_end.year==2008 or ob_end.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((ob_end - e2).days + 1)/n_days
                    if (state in observation_count_by_state): #if age already in dictionary
                        observation_count_by_state[state][ob_end.year-DATA_START_YEAR]+=ob_days
                    else: #if age not in dictionary
                        observation_count_by_state[state] = [0]*span
                        observation_count_by_state[state][ob_end.year-DATA_START_YEAR]+=ob_days
    print('Observation records processed.....................> {0}'.format(line_count))
    print('Observation records skipped.......................> {0}'.format(skip_count))
    return observation_count_by_state

def get_imputed_metavisits_probs_by_year (coded_imputed_mv, uncoded_imputed_mv, span):
    '''
    This function creates 3 dictionaries:
    One dictionary contains probabilties for coded metavisits by year.
    Second dictionary contains probabilties for non-coded metavisits by year.
    Third dictionary contains counts for coded metavisits by year.
    '''
    print ('\nCounts of different types of metavisits each year........>>>')
    coded_imputed_mv_probs_by_year = {} # probs for coded
    coded_imputed_mv_count_by_year = {} # count for coded
    uncoded_imputed_mv_probs_by_year = {} #probs for non-coded
    line_count = -1
    skip_count = 0

    ##start processing
    with gzip.open(mv_demography_file, 'rb') as mdf:
        for line in mdf:
            line_count+=1
            if (line_count%2500000==0):
                print('Number of records processed.................> {0}'.format(line_count))
            #vals = line.strip().split('\t')
            vals = line.decode('utf8').strip().split('\t') #for zipped file record
            if (line_count <=0): #fist line is header, get the index of columns
                for i,v in enumerate(vals):
                    if (v.strip(' ') == 'metavisit_occurrence_id'):
                        mv_idx = i
                    elif (v.strip(' ') == 'person_id'):
                        p_idx = i
                    elif (v.strip(' ') == 'metavisit_period'):
                        mvp_idx = i
                    elif (v.strip(' ') == 'yob'):
                        yob_idx = i
                    elif (v.strip(' ') == 'state'):
                        state_idx = i
                    elif (v.strip(' ') == 'gender'):
                        gender_idx = i
                    else:
                        print('Unknown column')
            else: #actual record
                mv = vals[mv_idx].strip(' ')
                state = vals[state_idx].strip(' ')
                date_pair = vals[mvp_idx].strip('[').strip(')').split(',')
                st_year = datetime.strptime(date_pair[0].strip(' '), '%Y-%m-%d')
                end_year = datetime.strptime(date_pair[1].strip(' '), '%Y-%m-%d')

                #coded metavisit
                if (mv in coded_imputed_mv):
                    for yr in range(st_year.year, end_year.year+1, 1):
                        if (yr in coded_imputed_mv_probs_by_year): #if year already there in dictionary
                            coded_imputed_mv_probs_by_year[yr]+=coded_imputed_mv[mv] #add prob_1
                            coded_imputed_mv_count_by_year[yr]+=1
                        else:   #if year not in dictionary
                            coded_imputed_mv_probs_by_year[yr] = coded_imputed_mv[mv] #add prob_1
                            coded_imputed_mv_count_by_year[yr] = 1
                #uncoded probables
                elif (mv in uncoded_imputed_mv):
                    for yr in range(st_year.year, end_year.year+1, 1):
                        if (yr in uncoded_imputed_mv_probs_by_year): #if year already there in dictionary
                            uncoded_imputed_mv_probs_by_year[yr]+=uncoded_imputed_mv[mv] #add prob_1
                        else: #if year not in dictionary
                            uncoded_imputed_mv_probs_by_year[yr] = uncoded_imputed_mv[mv]
                else:
                    #print ('Metavisit was not present in classifier output...{0}'.format(mv))
                    skip_count+=1

    print('Total records...{0}, Skipped records...{1}, Number of records processed...{2}'.format(line_count, skip_count, line_count-skip_count))
    return coded_imputed_mv_probs_by_year, uncoded_imputed_mv_probs_by_year, coded_imputed_mv_count_by_year


def get_observation_count_by_year(span, all_person_id):
    '''
    Denominator: Find the observation count in each year. If a person is observed for only
    x days in a year, add x/365 or x/366 to the count.
    '''
    print ('\nCount the observations each year by year..........>>>>')
    line_count = -1
    skip_count = 0
    observation_count_by_year = {}
    print ('Find number of persons observed each year....')
    with gzip.open(person_observation_file, 'rb') as pof:
        for line in pof:
            line_count+=1
            if (line_count%2000000 == 0):
                print('Observation records processed.....................> {0}'.format(line_count))
            vals = line.decode('utf8').strip().split('\t') #for zipped file record
            if (line_count <=0): #fist line is header, get the index of columns
                for i,v in enumerate(vals):
                    if (v.strip(' ') == 'person_id'):
                        p_idx = i
                    elif (v.strip(' ') == 'observation_period_start_date'):
                        obs_idx = i
                    elif (v.strip(' ') == 'observation_period_end_date'):
                        obe_idx = i
                    elif (v.strip(' ') == 'state'):
                        state_idx = i
                    elif (v.strip(' ') == 'yob'):
                        yob_idx = i
                    else:
                        print('Unknown column')
            else: #actual record
                #### START - Fractional counting for a year depending on number of days observed.
                # e.g. if observation started in 2003 and ended in 2007, add 1 to observation_count_by_state for 2004, 2005, and 2006
                state = vals[state_idx].strip(' ')
                pid = vals[p_idx].strip(' ')
                if (pid not in all_person_id):
                    skip_count+=1
                    continue
                if (state not in US_STATES):
                    state = 'OT' #other territories
                ob_start = datetime.strptime(vals[obs_idx].strip(' '), '%Y-%m-%d')
                ob_end = datetime.strptime(vals[obe_idx].strip(' '), '%Y-%m-%d')
                if (ob_start.year == ob_end.year):
                    if (ob_start.year==2004 or ob_start.year==2008 or ob_start.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((ob_end - ob_start).days + 1)/n_days
                    if (ob_start.year in observation_count_by_year): #if year already in dictionary
                        observation_count_by_year[ob_start.year]+=ob_days
                    else: #if year not in dictionary
                        observation_count_by_year[ob_start.year]=ob_days
                else:
                    for yr in range(ob_start.year+1, ob_end.year, 1):
                        if (yr in observation_count_by_year):
                            observation_count_by_year[yr]+=1
                        else:
                            observation_count_by_year[yr]=1

                    #special processing for start date
                    e1 = datetime.strptime(str(ob_start.year)+'-12-31','%Y-%m-%d') #end of the year
                    if (ob_start.year==2004 or ob_start.year==2008 or ob_start.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((e1 - ob_start).days + 1)/n_days
                    if (ob_start.year in observation_count_by_year): #if year already in dictionary
                        observation_count_by_year[ob_start.year]+=ob_days
                    else: #if age not in dictionary
                        observation_count_by_year[ob_start.year]=ob_days

                    #special processing for end date
                    e2 = datetime.strptime(str(ob_end.year)+'-01-01','%Y-%m-%d')
                    if (ob_end.year==2004 or ob_end.year==2008 or ob_end.year==2012 or ob_start.year==2016):
                        n_days = 366.0
                    else:
                        n_days = 365.0
                    ob_days = ((ob_end - e2).days + 1)/n_days
                    if (ob_end.year in observation_count_by_year): #if year already in dictionary
                        observation_count_by_year[ob_end.year]+=ob_days
                    else: #if year not in dictionary
                        observation_count_by_year[ob_end.year]=ob_days

    print('Observation records processed.....................> {0}'.format(line_count))
    print('Observation records skipped.......................> {0}'.format(skip_count))
    return observation_count_by_year

def generate_data_for_plot(coded_imputed_mv_probs, uncoded_imputed_mv_probs, coded_imputed_mv_count, observation_count, opfile, span):
    '''
    generate data for plots. this function generates data only for state and age.
    '''
    print ('\nCreate CSV file for plots..............>>>>')
    flds = ['X_axis', 'Coded/Imputed', 'Imputed incidence', 'Coded incidence']
    # write to the CSV file
    with open(opfile, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=flds)
        writer.writeheader()
        for k,v in observation_count.items(): #k=age, v=observation_count
            rec_dict = {}
            j=0
            #1st column
            rec_dict[flds[j]] = k

            ##if key is missing, initialize to 0. JUST TO AVOID KEYERROR
            if (k not in coded_imputed_mv_count):
                coded_imputed_mv_count[k] = [0]*span
                print ('key {0} not in coded_imputed_mv_count'.format(k))
            if (k not in coded_imputed_mv_probs):
                coded_imputed_mv_probs[k] = [0]*span
                print ('key {0} not in coded_imputed_mv_probs'.format(k))
            if (k not in uncoded_imputed_mv_probs):
                uncoded_imputed_mv_probs[k] = [0]*span
                print ('key {0} not in uncoded_imputed_mv_probs'.format(k))
            #2nd column
            j+=1
            try:
                rec_dict[flds[j]] = 100.0*sum(coded_imputed_mv_probs[k])/(sum(coded_imputed_mv_probs[k]) + sum(uncoded_imputed_mv_probs[k]))
            except:
                rec_dict[flds[j]] = 0
                print ('1.Something went wrong, control should not come here.!!!', k)
            #3rd column
            j+=1
            try:
                rec_dict[flds[j]] = 100.0*(sum(coded_imputed_mv_probs[k]) + sum(uncoded_imputed_mv_probs[k]))/sum(v)
            except:
                rec_dict[flds[j]] = 0
                print ('2.Something went wrong, control should not come here.!!!', k)
            #4th column
            j+=1
            try:
                rec_dict[flds[j]] = 100.0*(sum(coded_imputed_mv_count[k]))/sum(v)
            except:
                rec_dict[flds[j]] = 0
                print ('3.Something went wrong, control should not come here.!!!', k)
            writer.writerow(rec_dict)


def generate_data_for_plot_year(coded_imputed_mv_probs, uncoded_imputed_mv_probs, coded_imputed_mv_count, observation_count, opfile):
    '''
    generate data for plots by year
    '''
    print ('\nCreate CSV file for plots..............>>>>')
    flds = ['X_axis', 'Coded/Imputed', 'Imputed incidence', 'Coded incidence']
    # write to the CSV file
    with open(opfile, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=flds)
        writer.writeheader()
        for k,v in observation_count.items(): #k=age, v=observation_count
            rec_dict = {}
            j=0
            #1st column
            rec_dict[flds[j]] = k

            ##if key is missing, initialize to 0. JUST TO AVOID KEYERROR
            if (k not in coded_imputed_mv_count):
                coded_imputed_mv_count[k] = 0
                print ('key {0} not in coded_imputed_mv_count'.format(k))
            if (k not in coded_imputed_mv_probs):
                coded_imputed_mv_probs[k] = 0
                print ('key {0} not in coded_imputed_mv_probs'.format(k))
            if (k not in uncoded_imputed_mv_probs):
                uncoded_imputed_mv_probs[k] = 0
                print ('key {0} not in uncoded_imputed_mv_probs'.format(k))
            #2nd column
            j+=1
            try:
                rec_dict[flds[j]] = 100.0*coded_imputed_mv_probs[k]/(coded_imputed_mv_probs[k] + uncoded_imputed_mv_probs[k])
            except:
                rec_dict[flds[j]] = 0
                print ('1.Something went wrong, control should not come here.!!!', k)
            #3rd column
            j+=1
            try:
                rec_dict[flds[j]] = 100.0*(coded_imputed_mv_probs[k] + uncoded_imputed_mv_probs[k])/v
            except:
                rec_dict[flds[j]] = 0
                print ('2.Something went wrong, control should not come here.!!!', k)
            #4th column
            j+=1
            try:
                rec_dict[flds[j]] = 100.0*(coded_imputed_mv_count[k])/v
            except:
                rec_dict[flds[j]] = 0
                print ('3.Something went wrong, control should not come here.!!!', k)
            writer.writerow(rec_dict)

def main():
    '''
    Start of the program
    '''
    #get the parameter from command line argument
    parser = argparse.ArgumentParser()
    parser.add_argument('--field', required=True)
    parsed = parser.parse_args()
    print('XLS file will be created using {0}'.format(parsed.field))
    span = DATA_END_YEAR-DATA_START_YEAR+2 #adding extra 1 for the bad records
    #create a set of person_ids
    all_person_id = find_person_id_for_classified_metavisits()
    # Create 2 dictionaries: one for coded & another for non_coded
    coded_imputed_mv, uncoded_imputed_mv = select_metavisits_probs()

    #create dictionaries based on the command line field
    if (parsed.field == 'age'):
        # count of different types of metavisits each year by age.
        coded_imputed_mv_probs_by_age, uncoded_imputed_mv_probs_by_age, coded_imputed_mv_count_by_age = get_imputed_metavisits_probs_by_age(coded_imputed_mv,
                                                                                                                                                uncoded_imputed_mv,
                                                                                                                                                span)
        #Find the observation count each age
        observation_count_by_age = get_observation_count_by_age(span, all_person_id)
        #generate data for plot
        generate_data_for_plot(coded_imputed_mv_probs_by_age, uncoded_imputed_mv_probs_by_age, coded_imputed_mv_count_by_age, observation_count_by_age, plot_data_by_age, span)

    elif (parsed.field == 'state'):
        # count of different types of metavisits each year by state.
        coded_imputed_mv_probs_by_state, uncoded_imputed_mv_probs_by_state, coded_imputed_mv_count_by_state = get_imputed_metavisits_probs_by_state(coded_imputed_mv,
                                                                                                                                                        uncoded_imputed_mv,
                                                                                                                                                        span)
        #Find the observation count each state
        observation_count_by_state = get_observation_count_by_state(span, all_person_id)
        #generate data for plot
        generate_data_for_plot(coded_imputed_mv_probs_by_state, uncoded_imputed_mv_probs_by_state, coded_imputed_mv_count_by_state, observation_count_by_state, plot_data_by_state, span)

    elif (parsed.field == 'year'):
        # count of different types of metavisits each year by state.
        coded_imputed_mv_probs_by_year, uncoded_imputed_mv_probs_by_year, coded_imputed_mv_count_by_year = get_imputed_metavisits_probs_by_year(coded_imputed_mv,
                                                                                                                                                 uncoded_imputed_mv,
                                                                                                                                                 span)
        #Find the observation count each year
        observation_count_by_year = get_observation_count_by_year(span, all_person_id)
        print (observation_count_by_year)
        #generate data for plot
        generate_data_for_plot_year(coded_imputed_mv_probs_by_year, uncoded_imputed_mv_probs_by_year, coded_imputed_mv_count_by_year, observation_count_by_year, plot_data_by_year)
    print ('FINISHED!!!!')

###################LIST OF INPUT OUTPUT FILES###################################
DATA_START_YEAR = 2003 #Truven data start year
DATA_END_YEAR = 2016 #Truven data end year
US_STATES = ['AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY',
             'LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND',
             'OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY']
mv_prob_file = 'classifier_outputs/all_2003_16_metavisits_probs.txt'    #input
all_metavisits = 'train_files/all_metavisits/all_metavisits_2003_16.txt' #input
mv_demography_file = 'SQL/output/ccae2003_16_er_inpatient_suicidality_metavisit_period_data.tsv.gz' #input
person_observation_file = 'SQL/output/ccae2003_16_inp_er_person_observation_data.tsv.gz' #input
plot_data_by_age = 'main_outputs/ccae2003_16_plot_data_by_age_new.csv' #output
plot_data_by_state = 'main_outputs/ccae2003_16_plot_data_by_state_new.csv' #output
plot_data_by_year = 'main_outputs/ccae2003_16_plot_data_by_year_new.csv' #output

if __name__ == '__main__':
    '''
    This program generates the following counts for years 2003-2015/2003-2016:
        1. number of persons whose metavisits were classified as suicide metavisit (P1>=0.5).
        2. number of males and females whose metavisits were classified as suicide metavisit (P1>=0.5).
        3. how many persons had observation. If a person had observation only for x days in a year,
        x/365 or x/366 will be added to the count for that year.
        4. compute the counts of actual suicide and predicted suicide by yob for each year. Also,
        number of fractional observations by yob for each year.
    '''
    main()
