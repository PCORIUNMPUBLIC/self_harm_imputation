import workvariables as wv
import time
import numpy as np
import pickle
from scipy import sparse
import os
import xgboost as xgb
import random
import collections
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import matthews_corrcoef
from datetime import datetime


def load_csr_file_records():
    '''
    This function reads the CSR matrix from the saved file.
    '''
    print ('Loading .dat file into RAM....')
    with open(wv.all_metavisits_csr_file, 'rb') as pif:
        data1 = pickle.load(pif)
        print (data1.__getattr__)
    return data1, np.asarray(get_metavisit_labels(data1.shape[0]), dtype=np.int8), np.asarray(fetch_all_metavisits(data1.shape[0]))

def fetch_all_metavisits(no_of_records):
    '''
    This function reads the all_metavisits files and stores metavisits in a list.
    '''
    print ('Fetching all metavisits from file and storing them in a list... ')
    with open(wv.all_metavisits_file) as fmv:
        for line in fmv:
            metavisits = line.rstrip().split(',')
    print ('Number of metavisits in the file: {0}'.format(len(metavisits)))
    return (metavisits[:no_of_records])

def get_metavisit_labels(no_of_records):
    '''
    This function reads the metavisit_label file returns the labels.
    '''
    print ('Fetching labels for the metavisits...')
    with open(wv.all_labels_file) as fl:
        for line in fl:
            labels = line.rstrip().split(',')
    print ('Number of labels in the file: {0}'.format(len(labels)))
    return (labels[:no_of_records])

def evalmcc(preds, dtrain):
    THRESHOLD = 0.5
    labels = dtrain.get_label()
    return 'MCC', matthews_corrcoef(labels, preds >= THRESHOLD)

def classify_the_data (X, Y, metavisits_list):
    '''
    This function calls xgboost classifier to classify the data and compute
    several performance metrices.
    '''
    imp_features = {}
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    # compute indices for train and test data for 5-fold validation
    print ('Generating train and test indices for 5-fold validation....')
    v = np.random.permutation(X.shape[0])
    r5 = len(v) % 5
    i5 = 0
    for i in range(5):
        lv5 = int(len(v)/5)
        if(r5 > 0):
            lv5 = lv5+1
            r5 = r5-1
        teidx = v[i5:min(i5+lv5,len(v))]
        tridx = np.append(v[0:i5], v[min(i5+lv5,len(v)):len(v)])
        i5 = i5 + lv5

        print ('Performing fold number {0}'.format(i+1))
        print ('Train the classifier....')
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric=evalmcc, verbose=False)
        bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx])
        #bst = xgb.XGBClassifier().fit(X[tridx], Y[tridx])

        print ('Test the classifier....')
        preds = bst.predict_proba(X[teidx])  # this will return probabilities

        print ('Finding important features.............')
        imps = bst.get_booster().get_score(importance_type='gain')
        imp_features.update(imps)   # update dictionary i.e. if key is alreay present, update the value, else add new key

        print ('Storing the computed propabilities in a dictionary....')
        y_test = Y[teidx]
        for j in range(len(teidx)):
            metavisit_suicide_prob[metavisits_list[teidx[j]]] = (preds[j], y_test[j])

    return metavisit_suicide_prob, imp_features


def get_accuracy_auc_mcc_score(metavisit_suicide_prob):
    '''
    This function computes the accuracy, AUC, and MCC of the classifier.
    '''
    y_true = []
    y_pred_acc = []
    y_pred_auc = []
    for mv, pred in metavisit_suicide_prob.items():
        y_true.append(pred[1])
        y_pred_auc.append(pred[0][1])
        y_pred_acc.append(round(pred[0][1]))
    # evaluate predictions
    acc = accuracy_score(y_true, y_pred_acc)
    auc = roc_auc_score(y_true, y_pred_auc)
    mcc = matthews_corrcoef(y_true, y_pred_acc)
    print('Accuracy of the classifier is...............: {0}'.format(acc))
    print('MCC of the classifier is....................: {0}'.format(mcc))
    print('AUC for the classifier is...................: {0}'.format(auc))
    return acc, mcc, auc

def generate_report(classifier_performance):
    '''
    Using classifier_performance compute mean and deviation at 80% CI.
    '''
    for classifier, vals in classifier_performance.items():
        acc = []
        mcc = []
        auc = []
        for val in vals:
            acc.append(val[0])
            mcc.append(val[1])
            auc.append(val[2])
        acc = sorted(acc)
        mcc = sorted(mcc)
        auc = sorted(auc)
        print (classifier)
        print ('Min acc... {0}, Max acc...{1}, Mean acc...{2}, Deviation...{3}'.format(acc[1], acc[8], (acc[8]+acc[1])/2.0, (acc[8]-acc[1])/2.0))
        print ('Min mcc... {0}, Max mcc...{1}, Mean mcc...{2}, Deviation...{3}'.format(mcc[1], mcc[8], (mcc[8]+mcc[1])/2.0, (mcc[8]-mcc[1])/2.0))
        print ('Min auc... {0}, Max auc...{1}, Mean auc...{2}, Deviation...{3}'.format(auc[1], auc[8], (auc[8]+auc[1])/2.0, (auc[8]-auc[1])/2.0))


def main():
    '''
    This program loads metavisits from .dat file and their labels. It then uses xgboost for the classification.
    '''
    # load CSR data from .dat file
    print ('\nFetch data, labels, and metavisits from CSR files..........> ')
    X, Y, metavisits_list = load_csr_file_records()
    print ('Number of metavisit with label 1 is {0}'.format(np.sum(Y)))

    #classify the data
    print ('\nClassify the metavisits.............> ')
    classifier_performance = {}
    classifier = 'XGboost'
    for i in range(wv.ALL_MV_ITERATIONS):
        metavisit_suicide_prob, imp_features = classify_the_data (X, Y, metavisits_list)
        #Determine AUC, Accuracy and important features. Save metavisit classification results.
        print ('\nDetermine the performance metrics of the classifier........> ')
        acc, mcc, auc = get_accuracy_auc_mcc_score(metavisit_suicide_prob)
        if (classifier in classifier_performance):
            classifier_performance[classifier].append([acc, mcc, auc])
        else:
            classifier_performance[classifier] = [[acc, mcc, auc]]

    print (classifier_performance)
    fo = open(wv.all_mv_xgb_performance_file, 'w')
    fo.write(str(classifier_performance)) #save the results
    generate_report(classifier_performance)
    print ('\nProgram finished successfully!!!')

################################################################################
###                            Input Output files                            ###
################################################################################
#csr_file = 'train_files/all_metavisits/all_metavisits_csr_data_2003_16.dat'
#metavisits_file = 'train_files/all_metavisits/all_metavisits_2003_16.txt'
#labels_file = 'train_files/all_metavisits/all_labels_2003_16.txt'
#covfile = 'train_files/all_covariates_2003_16.txt'
#imp_covar_file = 'classifier_outputs/all_2003_16_important_covariates_'
#mv_probs_file = 'classifier_outputs/all_2003_16_metavisits_probs_'

if __name__ == '__main__':
    '''
    This program runs the XGboost classifier 10 times with the full dataset to compute
    the classification results (AUC, MCC, Accuracy) at 80% confidence interval.
    '''
    main()
