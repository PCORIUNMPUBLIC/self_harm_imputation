import workvariables as wv
import time
import numpy as np
import pickle
from scipy import sparse
import xgboost as xgb
import random
import collections
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import average_precision_score

def load_csr_file_records():
    '''
    This function reads the CSR matrix from the saved file.
    '''
    print ('Loading .dat file into RAM....>>>')
    with open(wv.all_metavisits_csr_file, 'rb') as pif:
        data1 = pickle.load(pif)
        print (data1.__getattr__)
    return data1, np.array(get_metavisit_labels(data1.shape[0]), dtype=np.int8), np.array(fetch_all_metavisits(data1.shape[0]))

def fetch_all_metavisits(no_of_records):
    '''
    This function reads the all_metavisits files and stores metavisits in a list.
    '''
    print ('Fetching all metavisits from file and storing them in a list... ')
    with open(wv.all_metavisits_file) as fmv:
        for line in fmv:
            metavisits = line.rstrip().split(',')
    print ('Number of metavisits in the file: {0}'.format(len(metavisits)))
    return (metavisits[:no_of_records])

def get_metavisit_labels(no_of_records):
    '''
    This function reads the metavisit_label file returns the labels.
    '''
    print ('Fetching labels for the metavisits...')
    with open(wv.all_labels_file) as fl:
        for line in fl:
            labels = line.rstrip().split(',')
    print ('Number of labels in the file: {0}'.format(len(labels)))
    return (labels[:no_of_records])

def split_train_validation_set(X_all, Y_all, metavisits_all):
    '''
    This function splits the data into train and validation sets. The records are
    selected randomly. 30% records represent validation set and 70% records represent
    train set.
    '''
    random_idx = [i for i in range(len(Y_all))]
    random.shuffle(random_idx)
    train_idx = random_idx[:int(len(Y_all)*0.7)]
    validation_idx = random_idx[int(len(Y_all)*0.7):]
    return X_all[train_idx], Y_all[train_idx], metavisits_all[train_idx], X_all[validation_idx], Y_all[validation_idx], metavisits_all[validation_idx]

def evalmcc(preds, dtrain):
    THRESHOLD = 0.5
    labels = dtrain.get_label()
    return 'MCC', matthews_corrcoef(labels, preds >= THRESHOLD)

def classify_the_train_data(X, Y, metavisits_list):
    '''
    This function calls xgboost classifier to classify the data and compute
    several performance metrices.
    '''
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    # compute indices for train and test data for 5-fold validation
    print ('Generating train and test indices for 5-fold validation....')
    v = np.random.permutation(X.shape[0])
    r5 = len(v) % 5
    i5 = 0
    for i in range(5):
        lv5 = int(len(v)/5)
        if(r5 > 0):
            lv5 = lv5+1
            r5 = r5-1
        teidx = v[i5:min(i5+lv5,len(v))]
        tridx = np.append(v[0:i5], v[min(i5+lv5,len(v)):len(v)])
        i5 = i5 + lv5

        print ('Performing fold number {0}'.format(i+1))
        print ('Train the classifier....')
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric=evalmcc, verbose=False)
        bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx])
        #bst = xgb.XGBClassifier().fit(X[tridx], Y[tridx])

        print ('Test the classifier....')
        preds = bst.predict_proba(X[teidx])  # this will return probabilities

        print ('Storing the computed propabilities in a dictionary....')
        y_test = Y[teidx]
        for j in range(len(teidx)):
            metavisit_suicide_prob[metavisits_list[teidx[j]]] = (preds[j], y_test[j])

    return metavisit_suicide_prob


def classify_the_validation_data (X_train, Y_train, X_val, Y_val, metavisits_val):
    '''
    This function trains the model using train data and then uses validation data
    to test the performance of the classifier.
    '''
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y_train)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    print ('Train the classifier....')
    #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric=evalmcc, verbose=False)
    bst = xgb.XGBClassifier(**param).fit(X_train, Y_train)
    #bst = xgb.XGBClassifier().fit(X[tridx], Y[tridx])

    print ('Test the classifier....')
    preds = bst.predict_proba(X_val)  # this will return probabilities

    print ('Storing the computed propabilities in a dictionary....')
    for j in range(len(Y_val)):
        metavisit_suicide_prob[metavisits_val[j]] = (preds[j], Y_val[j])

    return metavisit_suicide_prob


def get_accuracy_auc_mcc_score(metavisit_suicide_prob):
    '''
    This function computes the accuracy, AUC, and MCC of the classifier.
    '''
    y_true = []
    y_pred_acc = []
    y_pred_auc = []
    for mv, pred in metavisit_suicide_prob.items():
        y_true.append(pred[1])
        y_pred_auc.append(pred[0][1])
        y_pred_acc.append(round(pred[0][1]))
    # evaluate predictions
    print('Accuracy of the classifier is...............: {0}'.format(accuracy_score(y_true, y_pred_acc)))
    print('MCC of the classifier is....................: {0}'.format(matthews_corrcoef(y_true, y_pred_acc)))
    print('AUC for the classifier is...................: {0}'.format(roc_auc_score(y_true, y_pred_auc)))
    print('Avg precision-recall for the classifier is..: {0}'.format(average_precision_score(y_true, y_pred_auc)))

def main():
    '''
    start of the code
    '''
    # load CSR data from .dat file
    stime = time.time()
    print ('Fetch data, labels, and metavisit list from CSR files..........> ')
    X_all, Y_all, metavisits_all = load_csr_file_records()
    print ('Number of metavisit with label 1 is {0}'.format(np.sum(Y_all)))
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    #Generate test and validation set
    stime = time.time()
    print ('\nCreate train and validation sets.....>')
    X_train, Y_train, metavisits_train, X_val, Y_val, metavisits_val = split_train_validation_set(X_all, Y_all, metavisits_all)
    print ('Number of 1s in the validation set: {0}, number of 1s in test set:{1}'.format(np.sum(Y_val), np.sum(Y_train)))

    #classify the train data
    stime = time.time()
    print ('\nClassify the train metavisits.............> ')
    metavisit_suicide_prob = classify_the_train_data (X_train, Y_train, metavisits_train)
    #Determine AUC, Accuracy for train data (5-fold)
    print ('\nDetermine the performance metrics of the classifier with train set (5-fold CV)........> ')
    get_accuracy_auc_mcc_score(metavisit_suicide_prob)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    #classify the validation data
    stime = time.time()
    print ('\nClassify the validation metavisits.............> ')
    metavisit_suicide_prob = classify_the_validation_data (X_train, Y_train, X_val, Y_val, metavisits_val)
    #Determine AUC, Accuracy for train data (5-fold)
    print ('\nDetermine the performance metrics of the classifier with validation set........> ')
    get_accuracy_auc_mcc_score(metavisit_suicide_prob)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))
    print ('\nProgram finished successfully!!!')

################################################################################
###                            Input Output files                            ###
################################################################################
#all_csr_file = 'train_files/all_metavisits/all_metavisits_csr_data_2003_16.dat'
#all_metavisits_file = 'train_files/all_metavisits/all_metavisits_2003_16.txt'
#all_labels_file = 'train_files/all_metavisits/all_labels_2003_16.txt'


if __name__ == '__main__':
    '''
    This program splits the data into train and validation sets. The validation set
    (30% of the data) is generated randomly. The program runs 5-fold cross validation
    using the train set (70% of the data) and returns the classification results. Then
    it trains the model using trains data. The trained model is used on the validation
    set to find the classification results.
    '''
    main()
