import workvariables as wv
import time
import numpy as np
import pickle
from scipy import sparse
import os
import xgboost as xgb
import random
import collections
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import average_precision_score
from datetime import datetime


def load_csr_file_records():
    '''
    This function reads the CSR matrix from the saved file.
    '''
    print ('Loading .dat file into RAM....')
    with open(wv.bal_csr_file, 'rb') as pif:
        data1 = pickle.load(pif)
        print (data1.__getattr__)
    return data1, np.array(get_metavisit_labels(data1.shape[0]), dtype=np.int8), fetch_all_metavisits(data1.shape[0])

def fetch_all_metavisits(no_of_records):
    '''
    This function reads the all_metavisits files and stores metavisits in a list.
    '''
    print ('Fetching all metavisits from file and storing them in a list... ')
    with open(wv.bal_metavisits_file) as fmv:
        for line in fmv:
            metavisits = line.rstrip().split(',')
    print ('Number of metavisits in the file: {0}'.format(len(metavisits)))
    return (metavisits[:no_of_records])

def get_metavisit_labels(no_of_records):
    '''
    This function reads the metavisit_label file returns the labels.
    '''
    print ('Fetching labels for the metavisits...')
    with open(wv.bal_labels_file) as fl:
        for line in fl:
            labels = line.rstrip().split(',')
    print ('Number of labels in the file: {0}'.format(len(labels)))
    return (labels[:no_of_records])

def evalmcc(preds, dtrain):
    THRESHOLD = 0.5
    labels = dtrain.get_label()
    return 'MCC', matthews_corrcoef(labels, preds >= THRESHOLD)

def classify_the_data (X, Y, metavisits_list):
    '''
    This function calls xgboost classifier to classify the data and compute
    several performance metrices.
    '''
    imp_features = {}
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    # compute indices for train and test data for 5-fold validation
    print ('Generating train and test indices for 5-fold validation....')
    v = np.random.permutation(X.shape[0])
    r5 = len(v) % 5
    i5 = 0
    for i in range(5):
        lv5 = int(len(v)/5)
        if(r5 > 0):
            lv5 = lv5+1
            r5 = r5-1
        teidx = v[i5:min(i5+lv5,len(v))]
        tridx = np.append(v[0:i5], v[min(i5+lv5,len(v)):len(v)])
        i5 = i5 + lv5

        print ('Performing fold number {0}'.format(i+1))
        print ('Train the classifier....')
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric=evalmcc, verbose=False)
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx])
        bst = xgb.XGBClassifier().fit(X[tridx], Y[tridx])

        print ('Test the classifier....')
        preds = bst.predict_proba(X[teidx])  # this will return probabilities

        print ('Finding important features.............')
        imps = bst.get_booster().get_score(importance_type='gain')
        imp_features.update(imps)   # update dictionary i.e. if key is alreay present, update the value, else add new key

        print ('Storing the computed propabilities in a dictionary....')
        y_test = Y[teidx]
        for j in range(len(teidx)):
            metavisit_suicide_prob[metavisits_list[teidx[j]]] = (preds[j], y_test[j])

    return metavisit_suicide_prob, imp_features


def get_accuracy_auc_mcc_score(metavisit_suicide_prob):
    '''
    This function computes the accuracy, AUC, and MCC of the classifier.
    '''
    y_true = []
    y_pred_acc = []
    y_pred_auc = []
    for mv, pred in metavisit_suicide_prob.items():
        y_true.append(pred[1])
        y_pred_auc.append(pred[0][1])
        y_pred_acc.append(round(pred[0][1]))
    # evaluate predictions
    print('Accuracy of the classifier is...............: {0}'.format(accuracy_score(y_true, y_pred_acc)))
    print('MCC of the classifier is....................: {0}'.format(matthews_corrcoef(y_true, y_pred_acc)))
    print('AUC for the classifier is...................: {0}'.format(roc_auc_score(y_true, y_pred_auc)))
    print('Avg precision-recall for the classifier is..: {0}'.format(average_precision_score(y_true, y_pred_auc)))


def find_important_features(imp_features, iff):
    '''
    This function finds the features that were used by the classifier.
    '''
    # get all features/covariates in a list
    with open(wv.all_covfile) as cfl:
        for line in cfl:
            covars = line.strip().split(',')

    #get the concept_name for each concept_id
    print ('Creating concept_id_name dictionary with concept_id as key and concept_name as value....')
    lcount=-1
    c_id = 0
    c_name = 0
    concept_id_name_dict = {}
    with open(wv.concept_file) as cf:
        for line in cf:
            lcount+=1
            vals = line.strip().split('\t')
            if(lcount==0):
                for idx, col in enumerate(vals):
                    if (col == 'concept_id'):
                        c_id = idx
                    elif(col == 'concept_name'):
                        c_name = idx
            else:
                concept_id_name_dict[vals[c_id]] = vals[c_name]

    print ('Writing the important features to the file............')
    header_line = 'Concept_id ' + '\t' + 'Concept_name' + '\t' + 'Gain Score' + '\n\n'
    iff.write(header_line)
    ###START - Some housekeeping to avoid key-error.
    int_test = set('abcdefghijklmnopqrstuvwxyz')    #for user-defined concepts, there is no concept_name. Concept_ids are integers only.
    non_concepts = set(['2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016']) #satrt year of metavisit_period
    for i in range(100): #append age
        non_concepts.add(str(i))
    ###END - Some housekeeping to avoid key-error.
    for k in sorted(imp_features, key=imp_features.get, reverse=True): # sort the dictionary in decreasing order of value
        pos = int(k[1:])
        if (len(int_test.intersection(covars[pos])) > 0 or covars[pos] in non_concepts):
            line = covars[pos] + '\t' + '-' + '\t' + str(imp_features[k]) + '\n'
        else:
            line = covars[pos] + '\t' + concept_id_name_dict[covars[pos]] + '\t' + str(imp_features[k]) + '\n'
        iff.write(line)
    iff.close()

def save_classification_results_in_a_file(metavisit_suicide_self_harm_prob, mpf):
    '''
    This function writes metavisits and their corresponding true class and predicted
    probabilities for class 0 and class 1 to a file.
    '''
    # write metavisit and its probabilty to an output file
    for mvisit, pred in metavisit_suicide_self_harm_prob.items():
        line = str(mvisit) + ' : ' + str(pred[0]) +  ' : ' + str(pred[1]) + '\n'
        mpf.write(line)


def main():
    '''
    This program loads metavisits from .dat file and their labels. It then uses xgboost for the classification.
    '''
    # load CSR data from .dat file
    stime = time.time()
    print ('Fetch data, labels, and metavisit list from CSR files..........> ')
    X, Y, metavisits_list = load_csr_file_records()
    print ('Number of metavisit with label 1 is {0}'.format(np.sum(Y)))
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    #classify the data
    stime = time.time()
    print ('\nClassify the metavisits.............> ')
    metavisit_suicide_prob, imp_features = classify_the_data (X, Y, metavisits_list)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    #Determine AUC, Accuracy and important features. Save metavisit classification results.
    stime = time.time()
    print ('\nDetermine the performance metrics of the classifier........> ')
    get_accuracy_auc_mcc_score(metavisit_suicide_prob)

    print ('\nCreate a file with important features................> ')
    iff = open(wv.bal_imp_covar_file, 'w') # file to store important features
    find_important_features(imp_features, iff)

    print ('\nSave classification results in a file...........>')
    mpf = open(wv.bal_mv_probs_file, 'w')   #file to store metavisits and their corresponding probabilties
    save_classification_results_in_a_file(metavisit_suicide_prob, mpf)
    print ('Time taken by this step: {0} seconds'.format(time.time()-stime))

    print ('\nProgram finished successfully!!!')

################################################################################
###                            Input Output files                            ###
################################################################################
#bal_csr_file = 'train_files/balanced_data/bal_metavisits_csr_data_2003_16.dat'
#bal_metavisits_file = 'train_files/balanced_data/bal_metavisits_2003_16.txt'
#bal_labels_file = 'train_files/balanced_data/bal_labels_2003_16.txt'
#covfile = 'train_files/all_covariates_2003_16.txt'
#imp_covar_file = 'classifier_outputs/bal_important_covariates_'
#mv_probs_file = 'classifier_outputs/bal_metavisits_probs_'

if __name__ == '__main__':
    '''
    This program train and test XGboost classifier using balanced inpatient/ER metavisits
    data(148K). The program loads CSR data from the saved .dat file and then generates data,
    labels and list of metavisits. Data, labels, and list of metavisits are passed to XGboost
    classifier to train and test the model using 5-fold cross-validation.
    '''
    main()
