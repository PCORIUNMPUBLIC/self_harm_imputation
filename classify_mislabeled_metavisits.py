import workvariables as wv
import collections
import time
import pickle
import numpy as np
from scipy import sparse
import random as rnd
import xgboost as xgb
from copy import deepcopy
import argparse
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import average_precision_score
from datetime import datetime
from sklearn.metrics import confusion_matrix

def load_csr_file_records(argParam):
    '''
    This function reads the CSR matrix from the saved file.
    '''
    print ('Loading .dat file into RAM....')
    if (argParam == 'bal'):
        with open(wv.bal_csr_file, 'rb') as pif:
            data1 = pickle.load(pif)
            print (data1.__getattr__)
    elif (argParam == 'all'):
        with open(wv.all_metavisits_csr_file, 'rb') as pif:
            data1 = pickle.load(pif)
            print (data1.__getattr__)
    return data1, np.array(get_metavisit_labels(data1.shape[0], argParam), dtype=np.int8), np.array(fetch_all_metavisits(data1.shape[0], argParam))

def fetch_all_metavisits(no_of_records, argParam):
    '''
    This function reads the all_metavisits files and stores metavisits in a list.
    '''
    print ('Fetching all metavisits from file and storing them in a list... ')
    if (argParam == 'bal'):
        with open(wv.bal_metavisits_file) as fmv:
            for line in fmv:
                metavisits = line.rstrip().split(',')
    elif (argParam == 'all'):
        with open(wv.all_metavisits_file) as fmv:
            for line in fmv:
                metavisits = line.rstrip().split(',')
    print ('Number of metavisits in the file: {0}'.format(len(metavisits)))
    return (metavisits[:no_of_records])

def get_metavisit_labels(no_of_records, argParam):
    '''
    This function reads the metavisit_label file returns the labels.
    '''
    print ('Fetching labels for the metavisits...')
    if (argParam == 'bal'):
        with open(wv.bal_labels_file) as fl:
            for line in fl:
                labels = line.rstrip().split(',')
    elif (argParam == 'all'):
        with open(wv.all_labels_file) as fl:
            for line in fl:
                labels = line.rstrip().split(',')
    print ('Number of labels in the file: {0}, number of 1s in the file:{1}'.format(len(labels), labels.count('1')))
    return (labels[:no_of_records])

def change_labels_of_metavisits(Y, to_change):
    '''
    This function flips the labels of half of the metavists (1 to 0 OR 0 to 1) depending on the parameter passed.
    '''
    how_many_to_change = int(np.sum(Y)/2)    #half
    if (to_change == 0): #change 0 to 1
        print ('\nChanging labels from 0 to 1 for {0} metavists..........>>>'.format(how_many_to_change))
        pos_of_0 = list(np.where(Y==0)[0])  #np.where gives tuple. 1st element is an np array
        random_indices = rnd.sample(pos_of_0, how_many_to_change)  #randomly select half of them
        Y[random_indices] = 1
    else:   #change 1 to 0
        print ('\nChanging labels from 1 to 0 for {0} metavists..........>>>'.format(how_many_to_change))
        pos_of_1 = list(np.where(Y==1)[0])  #np.where gives tuple. 1st element is an np array
        random_indices = rnd.sample(pos_of_1, how_many_to_change)  #randomly select half of them
        Y[random_indices] = 0
    return np.array(Y, dtype=np.int8), random_indices

def evalmcc(preds, dtrain):
    THRESHOLD = 0.5
    labels = dtrain.get_label()
    return 'MCC', matthews_corrcoef(labels, preds >= THRESHOLD)


def classify_the_data(X, Y, metavisits_list, Y_orig):
    '''
    This function calls xgboost classifier to classify the data and compute
    several performance metrices.
    '''
    imp_features = {}
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1}

    # compute indices for train and test data for 5-fold validation
    print ('Generating train and test indices for 5-fold validation....')
    v = np.random.permutation(X.shape[0])
    r5 = len(v) % 5
    i5 = 0
    for i in range(5):
        lv5 = int(len(v)/5)
        if(r5 > 0):
            lv5 = lv5+1
            r5 = r5-1
        teidx = v[i5:min(i5+lv5,len(v))]
        tridx = np.append(v[0:i5], v[min(i5+lv5,len(v)):len(v)])
        i5 = i5 + lv5

        print ('Performing fold number {0}'.format(i+1))
        print ('Train the classifier....')
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric=evalmcc, verbose=False)
        #bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx], eval_set=[(X[tridx], Y[tridx])], eval_metric='error', verbose=False)
        bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx])

        print ('Test the classifier....')
        preds = bst.predict_proba(X[teidx])  # this will return probabilities

        print ('Finding important features.............')
        imps = bst.get_booster().get_score(importance_type='gain')
        imp_features.update(imps)   # update dictionary i.e. if key is alreay present, update the value, else add new key

        print ('Store the computed propabilities in a dictionary...')
        y_test = Y[teidx]
        for j in range(len(teidx)):
            metavisit_suicide_prob[metavisits_list[teidx[j]]] = (preds[j], y_test[j], Y_orig[teidx[j]])
    return metavisit_suicide_prob, imp_features

def get_accuracy_auc_mcc_score(metavisit_suicide_prob):
    '''
    This function computes the accuracy, AUC, and MCC of the classifier.
    '''
    y_true = []
    y_orig = []
    y_pred_acc = []
    y_pred_auc = []
    for mv, pred in metavisit_suicide_prob.items():
        y_true.append(pred[1])  #true class (flipped class)
        y_orig.append(pred[2])  #origianl unchanged class
        y_pred_auc.append(pred[0][1]) #class '1' probabilty for AUC
        y_pred_acc.append(round(pred[0][1])) # predicted probabilty for ACC
    # evaluate predictions
    print('Accuracy of the classifier is (flipped, origial)...............: {0}, {1}'.format(accuracy_score(y_true, y_pred_acc), accuracy_score(y_orig, y_pred_acc)))
    print('MCC of the classifier is (flipped, origial)....................: {0}, {1}'.format(matthews_corrcoef(y_true, y_pred_acc), matthews_corrcoef(y_orig, y_pred_acc)))
    print('AUC for the classifier is (flipped, origial)...................: {0}, {1}'.format(roc_auc_score(y_true, y_pred_auc), roc_auc_score(y_orig, y_pred_auc)))
    print('Avg precision-recall for the classifier is (flipped, origial)..: {0}, {1}'.format(average_precision_score(y_true, y_pred_auc), average_precision_score(y_orig, y_pred_auc)))
    (tn, fp, fn, tp) = confusion_matrix(y_true, y_pred_acc).ravel()
    print('tn, fp, fn, tp using flipped labels...', (tn, fp, fn, tp))
    (tn, fp, fn, tp) = confusion_matrix(y_orig, y_pred_acc).ravel()
    print('tn, fp, fn, tp using original labels...', (tn, fp, fn, tp))

def check_recovery_rate (metavisit_suicide_prob, flipped_metavisits, flipped_label):
    '''
    This function calculates the percentage of recovery.
    '''
    recovered_count = 0 #how many recovered
    for mv, pred in metavisit_suicide_prob.items():
        if (mv in flipped_metavisits and round(pred[0][1]) == flipped_label):
            recovered_count+=1
    print ('% of metavists recovered.............. {0}'.format(100.0*recovered_count/len(flipped_metavisits)))


#def save_classification_results_in_a_file(metavisit_suicide_self_harm_prob, mpf):
#    '''
#    This function writes metavisits and their corresponding true class and predicted
#    probabilities for class 0 and class 1 to a file.
#    '''
#    # write metavisit and its probabilty to an output file
#    for mvisit, pred in metavisit_suicide_self_harm_prob.items():
#        #line = str(mvisit) + ' : ' + str(pred[0]) +  ' : ' + str(pred[1]) + '\n'
#        line = str(mvisit) + ' : ' + str(pred[0]) +  ' : ' + str(pred[1]) + ' : ' + str(pred[2]) + '\n'
#        mpf.write(line)


def main():
    '''
    This is the main function to call all other functions.
    '''
    #get the parameter from command line argument. command line argument is used to
    #select the dataset (full or balanced).
    parser = argparse.ArgumentParser()
    parser.add_argument('--field', required=True)
    parsed = parser.parse_args()
    if (parsed.field not in ['bal','all']):
        print ('Wrong argument passed, correct values are bal and all ...!!!')
        exit()
    print ('Recovery of mislabeled data using {0} dataset.....>>>>'.format(parsed.field))
    if (parsed.field == 'bal'):
        #fetch records from CSR matrix, metavisit file and labels file to create
        #data and label for the classifier.
        print ('Get CSR data, metavists and their labels....>')
        X, Y, metavisits_list = load_csr_file_records(parsed.field)
        Y_orig = deepcopy(Y) # this will make sure original Y remains constant

        #Classify metavists without changing labels
        print ('Classify the original data.............!!!')
        metavisit_suicide_prob, imp_features = classify_the_data (X, Y, metavisits_list, Y_orig)
        print ('Number of elements in metavisit_suicide_prob.....{0}'.format(len(metavisit_suicide_prob)))
        print ('\nDetermine the performance metrics of the classifier........> ')
        get_accuracy_auc_mcc_score(metavisit_suicide_prob)

        ##flip labels of half of the metavists from 1 to 0
        flipped_label = 1
        Y_flipped, flipped_indices = change_labels_of_metavisits(Y, flipped_label)
        print ('Number of metavisits...{0} class 1 metavists (original, flipped).... {1}, {2}'.format(len(Y_flipped), np.sum(Y_orig), np.sum(Y_flipped)))
        #Classify metavists after changing labels
        print ('Classify the flipped data.............!!!')
        metavisit_suicide_prob, imp_features = classify_the_data (X, Y_flipped, metavisits_list, Y_orig)
        print ('Number of elements in metavisit_suicide_prob.....{0}'.format(len(metavisit_suicide_prob)))
        print ('\nDetermine the performance metrics of the classifier after mislabeling 1 to 0........> ')
        get_accuracy_auc_mcc_score(metavisit_suicide_prob)
        flipped_metavisits = metavisits_list[flipped_indices]
        check_recovery_rate (metavisit_suicide_prob, flipped_metavisits, flipped_label)

        ##flip labels of half of the metavists from 0 to 1
        Y = deepcopy(Y_orig) # copy the origianl Y values
        flipped_label = 0
        Y_flipped, flipped_indices = change_labels_of_metavisits(Y, flipped_label)
        print ('Number of metavisits...{0} class 1 metavists (original, flipped).... {1}, {2}'.format(len(Y_flipped), np.sum(Y_orig), np.sum(Y_flipped)))
        #Classify metavists after changing labels
        print ('Classify the flipped data.............!!!')
        metavisit_suicide_prob, imp_features = classify_the_data (X, Y_flipped, metavisits_list, Y_orig)
        print ('Number of elements in metavisit_suicide_prob.....{0}'.format(len(metavisit_suicide_prob)))
        print ('\nDetermine the performance metrics of the classifier after mislabeling 0 to 1........> ')
        get_accuracy_auc_mcc_score(metavisit_suicide_prob)
        flipped_metavisits = metavisits_list[flipped_indices]
        check_recovery_rate (metavisit_suicide_prob, flipped_metavisits, flipped_label)

    elif (parsed.field == 'all'):
        #fetch records from CSR matrix, metavisit file and labels file to create
        #data and label for the classifier.
        print ('Get CSR data, metavists and their labels....>')
        X, Y, metavisits_list = load_csr_file_records(parsed.field)
        Y_orig = deepcopy(Y) # this will make sure original Y remains constant

        ##flip labels of half of the metavists from 1 to 0
        flipped_label = 1
        Y_flipped, flipped_indices = change_labels_of_metavisits(Y, flipped_label)
        print ('Number of metavisits...{0} class 1 metavists (original, flipped).... {1}, {2}'.format(len(Y_flipped), np.sum(Y_orig), np.sum(Y_flipped)))
        ###Classify metavists after changing labels
        print ('Classify the flipped data.............!!!')
        metavisit_suicide_prob, imp_features = classify_the_data (X, Y_flipped, metavisits_list, Y_orig)
        print ('Number of elements in metavisit_suicide_prob.....{0}'.format(len(metavisit_suicide_prob)))
        print ('\nDetermine the performance metrics of the classifier........> ')
        get_accuracy_auc_mcc_score(metavisit_suicide_prob)
        flipped_metavisits = metavisits_list[flipped_indices]
        check_recovery_rate (metavisit_suicide_prob, flipped_metavisits, flipped_label)

    print ('\nTHE END: the program finished successfully')

####################### List of input and output files #########################
#bal_csr_file = 'train_files/balanced_data/bal_metavisits_csr_data_2003_16.dat'  #input
#bal_metavisits_file = 'train_files/balanced_data/bal_metavisits_2003_16.txt'    #input
#bal_labels_file = 'train_files/balanced_data/bal_labels_2003_16.txt'    #input
#all_csr_file = 'train_files/all_metavisits/all_metavisits_csr_data_2003_16.dat'  #input
#all_metavisits_file = 'train_files/all_metavisits/all_metavisits_2003_16.txt'    #input
#all_labels_file = 'train_files/all_metavisits/all_labels_2003_16.txt'    #input
#covfile = 'train_files/all_covariates_2003_16.txt'  #input
#mv_probs_file = 'classifier_outputs/all_mislabeled_metavisits_probs_'     #output
################################################################################

if __name__ == '__main__':
    '''
    This program randomly mislabels 50% of class 1 meta-visits to class 0, trains the model
    on the mislabeled data, and computes the classification results using the origianl
    labels. For the balanced dataset, it also randomly mislabels 50% of class 0 meta-visits
    to class 1.
    '''
    main()
