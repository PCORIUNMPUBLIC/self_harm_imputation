# E-codes that should not be included into list of covariates
excluded_concepts = ['21300', '21301', '21302', '21303', '21304', '21305', '21306', '21307', '21308', '21309', '21310', '21311', '21312',
                     '21313', '1575705', '1575706', '1575707', '1575708', '1575709', '1575710', '1575711', '1575712', '35223833', '35223834',
                     '35223835', '35223836', '35223837', '35223838', '35223839', '35223840', '35223841', '35223842', '35223843', '35223844',
                     '35223845', '35223846', '35223847', '35223848', '35223849', '35223850', '35223851', '35223852', '35223853', '35223854',
                     '35223855', '35223856', '35223857', '35223858', '35223859', '35223860', '35223861', '35223862', '35223863', '35223864',
                     '35223865', '35223866', '35223867', '35223868', '35223869', '35223870', '35223871', '35223872', '35223873', '35223874',
                     '35223875', '35223876', '35223877', '35223878', '35223879', '35223880', '35223881', '35223882', '35223883', '35223884',
                     '35223885', '35223886', '35223887', '35223888', '35223889', '35223890', '35223891', '35223892', '35223893', '35223894',
                     '35223895', '35223896', '35223897', '35223898', '35223899', '35223900', '35223901', '35223902', '35223903', '35223904',
                     '35223905', '35223906', '35223907', '35223908', '35223909', '35223910', '35223911', '35223912', '35223913', '35223914',
                     '35223915', '35223916', '35223917', '35223918', '35223919', '35223920', '35223921', '35223922', '35223923', '35223924',
                     '35223925', '35223926', '35223927', '35223928', '35223929', '35223930', '35223931', '35223932', '35223933', '35223934',
                     '35223935', '35223936', '35223937', '35223938', '35223939', '35223940', '35223941', '35223942', '35223943', '35223944',
                     '35223945', '35223946', '35223947', '35223948', '35223949', '35223950', '35223951', '35223952', '35223953', '35223954',
                     '35223955', '35223956', '35223957', '35223958', '35223959', '35223960', '35223961', '44820346', '44820347', '44820348',
                     '44821472', '44821473', '44821474', '44822618', '44822619', '44822620', '44822621', '44823755', '44823756', '44823757',
                     '44823758', '44823759', '44824923', '44826117', '44826118', '44826119', '44826120', '44826121', '44826122', '44826123',
                     '44827266', '44827267', '44827268', '44827269', '44827270', '44828447', '44828448', '44828449', '44828450', '44829556',
                     '44829557', '44829558', '44830707', '44830708', '44830709', '44830710', '44830711', '44831875', '44833012', '44833013',
                     '44833014', '44834194', '44834195', '44834196', '44834197', '44835383', '44835384', '44836573', '44837719', '44837720',
                     '44837721', '45537469', '45537470', '45537471', '45537472', '45537473', '45537474', '45537475', '45537476', '45537477',
                     '45537478', '45537479', '45537480', '45537481', '45542262', '45542263', '45542264', '45542265', '45547148', '45547149',
                     '45547150', '45547151', '45547152', '45547153', '45547155', '45547156', '45551944', '45551945', '45551946', '45551947',
                     '45551948', '45551950', '45551951', '45551952', '45551954', '45551955', '45551956', '45551957', '45551958', '45551959',
                     '45551960', '45556675', '45556676', '45556677', '45556678', '45556679', '45556680', '45561466', '45561467', '45561469',
                     '45561470', '45561471', '45561472', '45561473', '45561474', '45561475', '45566261', '45566262', '45566263', '45566264',
                     '45566265', '45566266', '45566267', '45566268', '45566269', '45566270', '45566271', '45566272', '45566273', '45566274',
                     '45566275', '45566276', '45571182', '45571183', '45571184', '45571185', '45571186', '45571187', '45571188', '45571189',
                     '45571190', '45571191', '45571192', '45571193', '45575994', '45575995', '45575996', '45575997', '45575998', '45575999',
                     '45576000', '45576001', '45576002', '45576003', '45576004', '45576005', '45580865', '45580866', '45580867', '45580868',
                     '45580869', '45580870', '45580871', '45580872', '45580873', '45585662', '45585663', '45585664', '45585665', '45585666',
                     '45585667', '45585668', '45585669', '45585670', '45585671', '45585672', '45590546', '45590547', '45590548', '45590549',
                     '45590550', '45590552', '45590553', '45590554', '45595331', '45595332', '45595333', '45595334', '45595335', '45595336',
                     '45595337', '45595338', '45595339', '45595340', '45595341', '45595342', '45600200', '45600201', '45600202', '45600203',
                     '45600204', '45600205', '45600206', '45600207', '45604953', '45604954', '45604955', '45604956', '45604957', '45604958',
                     '45604959', '45604960', '45604961', '45609786', '45609787', '45609788', '45609789', '45609790', '45609791', '45609792',
                     '443281','40421994','438331','439454','4219484','444362','437769','4085349','439235','435174','440925','4318639','436605',
                     '432507','436593','439556','435148','443255','439555','434849','435446','443280','435433','436592','440282',
                     'diagnoses;mental_comorb;self_harm;']
# domain list
allowed_domains = ['Observation', 'Generic', 'Drug', 'Condition', 'Injury', 'Procedure']

# Input files for concepts
icd9_icd10Proc_file = '/home/pkumar/Suicidality_new/main_inputs/icd9_icd10Proc.csv'
cpt4_icd10_file = '/home/pkumar/Suicidality_new/main_inputs/cpt4_icd10.txt'
concept_file = '/home/pkumar/Suicidality_new/main_inputs/ccae2003_2016_concept.tsv'
concept_ancestor_file = '/home/pkumar/Suicidality_new/main_inputs/ccae2003_2016_concept_ancestor.tsv'
mv_person_file = '/home/pkumar/Suicidality_new/SQL/output/ccae2003_16_er_inpatient_suicidality_metavisit_period_data.tsv.gz'

#Input and output files machine learning and covariates generation codes
SQLopfile = '/home/pkumar/Suicidality_new/main_inputs/ccae2003_16_er_inpatient_suicidality_metavisits.tsv.gz'
all_metavisits_cov_file = '/home/pkumar/Suicidality_new/main_outputs/all_metavisits_2003_16_with_covariates.txt'
all_metavisits_csr_file = '/home/pkumar/Suicidality_new/train_files/full_data/all_metavisits_csr_data_2003_16.dat'
all_metavisits_file = '/home/pkumar/Suicidality_new/train_files/full_data/all_metavisits_2003_16.txt'
all_labels_file = '/home/pkumar/Suicidality_new/train_files/full_data/all_labels_2003_16.txt'
all_covfile = '/home/pkumar/Suicidality_new/train_files/all_covariates_2003_16.txt'
all_covfile1 = '/home/pkumar/Suicidality_new/train_files/all_covariates_without_ancestors.txt'
all_imp_covar_file = '/home/pkumar/Suicidality_new/classifier_outputs/all_2003_16_important_covariates.txt'
all_mv_probs_file = '/home/pkumar/Suicidality_new/classifier_outputs/all_2003_16_metavisits_probs.txt'
#10 iterations with full dataset
ALL_MV_ITERATIONS = 10 # no. of time XGboost needs to be run with full dataset
all_mv_xgb_performance_file = '/home/pkumar/Suicidality_new/classifier_outputs/xgb_performance_acc_mcc_auc.txt'
#for balanced dataset
bal_csr_file = '/home/pkumar/Suicidality_new/train_files/balanced_data/bal_metavisits_csr_data_2003_16.dat'
bal_metavisits_file = '/home/pkumar/Suicidality_new/train_files/balanced_data/bal_metavisits_2003_16.txt'
bal_labels_file = '/home/pkumar/Suicidality_new/train_files/balanced_data/bal_labels_2003_16.txt'
bal_imp_covar_file = '/home/pkumar/Suicidality_new/classifier_outputs/bal_2003_16_important_covariates.txt'
bal_mv_probs_file = '/home/pkumar/Suicidality_new/classifier_outputs/bal_2003_16_metavisits_probs.txt'
#100 iterations with balanced dataset
BAL_MV_ITERATIONS = 100 # no. of time XGboost needs to be run with balanced dataset
bal_mv_100_iter_probs_file = '/home/pkumar/Suicidality_new/classifier_outputs/metavisits_probs_using_'
bal_mv_classifiers_performance_file = '/home/pkumar/Suicidality_new/classifier_outputs/classifiers_performance_acc_mcc_auc.txt'
#Full-factorial analysis
full_fact_matrix_file = '/home/pkumar/Suicidality_new/classifier_outputs/full_factorial_matrix_mcc_auc.csv'
full_fact_classification_result_file = '/home/pkumar/Suicidality_new/classifier_outputs/full_factorial_acc_mcc_auc.txt'
full_fact_metavisits_probs_file = '/home/pkumar/Suicidality_new/classifier_outputs/bal_metavisits_probs_ff.txt'
#Coding-bias model
selected_imp_covar_file = '/home/pkumar/Suicidality_new/classifier_outputs/select95p_important_covariates.txt'
selected_mv_prob_file = '/home/pkumar/Suicidality_new/classifier_outputs/select95p_metavisits_probs.txt'
SELECTED_THRESHOLD = 0.95
#PER-Person-model
pp_imp_covar_file = 'classifier_outputs/per_person_2003_16_important_covariates.txt'
pp_mv_probs_file = 'classifier_outputs/per_person_2003_16_metavisits_probs.txt'

