create temporary table bob as (
		SELECT *
		  FROM ccae2003_2016.concept
);

\copy bob to '/home/pkumar/Suicidality_new/main_inputs/ccae2003_2016_concept.tsv' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';	


create temporary table bob1 as (
		SELECT *
		  FROM ccae2003_2016.concept_ancestor
);

\copy bob1 to '/home/pkumar/Suicidality_new/main_inputs/ccae2003_2016_concept_ancestor.tsv' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';
