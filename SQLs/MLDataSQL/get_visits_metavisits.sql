-- create a file with all metavisits where visit_type is inpatient or emergency (2003-16)
create temporary table bob as (
	  SELECT X1.metavisit_occurrence_id, X1.concept_ids, X1.concept_set_name, 
	         X2.visit_types, X2.metavisit_period, 
	         X3.gender, extract(year from (lower(X2.metavisit_period)))- extract(year from X3.birth_date) as age
	    FROM ccae2003_2016_lambert_suicidality.events_of_interest X1, 
	         ccae2003_2016_lambert_suicidality.metavisits_of_interest X2, 
	         ccae2003_2016_lambert_suicidality.cohort_of_interest X3
	   WHERE X1.metavisit_occurrence_id  = X2.metavisit_occurrence_id 
	     AND X2.person_id = X3.person_id
	     AND ('inpatient' = ANY(X2.visit_types)  OR 'emergency' = ANY(X2.visit_types))
	   ORDER BY X1.metavisit_occurrence_id
);
\copy bob to '/home/pkumar/Suicidality_new/main_inputs/ccae2003_16_er_inpatient_suicidality_metavisits.tsv' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';	



