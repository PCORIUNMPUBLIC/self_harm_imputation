-- fetch age, gender, and location data each metavisit (2003-16)
create temporary table pk1 as (
  SELECT DISTINCT X1.metavisit_occurrence_id, X1.person_id, X1.metavisit_period, X2.year_of_birth as yob, X3.state, 
    CASE WHEN X2.gender_concept_id = 8507 then 'male' 
         WHEN X2.gender_concept_id = 8532 then 'female' 
         ELSE 'None' 
    END as gender
	FROM ccae2003_2016_lambert_suicidality.metavisits_of_interest X1, 
	     ccae2003_2016.person X2, 
	     ccae2003_2016.location X3
   WHERE X1.person_id = X2.person_id
     AND X2.location_id = X3.location_id
     AND ('inpatient' = ANY(X1.visit_types) OR 'emergency' = ANY(X1.visit_types))
);
\copy pk1 to '/home/pkumar/Suicidality_new/SQL/output/ccae2003_16_er_inpatient_suicidality_metavisit_period_data.tsv' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b'; 



-- This SQL selects the obseration data for each person present in the cohort_of_interest table (2003-16)
create temporary table pk2 as (
SELECT DISTINCT X1.person_id, X1.observation_period_start_date, X1.observation_period_end_date, X3.state, extract(year from X2.birth_date) as yob
  FROM ccae2003_2016.observation_period X1, 
	   ccae2003_2016_lambert_suicidality.cohort_of_interest X2,
	   ccae2003_2016.location X3,
	   ccae2003_2016.person X4,
	   ccae2003_2016_lambert_suicidality.metavisits_of_interest X5 
 WHERE X1.person_id = X2.person_id
   AND X2.person_id = X4.person_id
   AND X4.person_id = X5.person_id
   AND X4.location_id = X3.location_id
   AND ('inpatient' = ANY(X5.visit_types)  OR 'emergency' = ANY(X5.visit_types)) 
 --ORDER BY X1.person_id 
);
\copy pk2 to '/home/pkumar/Suicidality_new/SQL/output/ccae2003_16_inp_er_person_observation_data.tsv' WITH DELIMITER E'\t' CSV HEADER QUOTE E'\b';	

