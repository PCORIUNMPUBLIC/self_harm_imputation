# Self harm imputation

Source code for data extraction and analysis for JAMIA publication: **Imputation and characterization of uncoded self-harm in major mental illness using machine learning**

*Praveen Kumar; Anastasiya Nestsiarovich, M.D., Ph.D.; Stuart J. Nelson, M.D.; Berit Kerner, M.D.; Douglas J. Perkins, Ph.D.; Christophe G. Lambert, Ph.D.*

## Introduction

This Gitlab repository contains all Python and SQL codes that were used for this study. Using the ETL-CDMBuilder, the IBM MarketScan data were converted into OMOP-CDMv5 format. 
BuildingBlocks SQLs were executed to create several tables of interest. MLData SQLs were run on those tables of interest to extract data for different machine learning (ML) models. 
Python codes were used for the XGboost and other ML algorithms, and to generate the data to characterize the coded vs. imputed self-harm by different parameters (e.g Age, sex, state, etc.). 

![alt text](overall_steps.png)

## Dependencies

The Python codes use these libraries: *pickle, numpy, scipy, scikit-learn, xgboost*

All python codes have been tested on Python3 installed on Linux machine. So, the use of Python3 on Linux machine is recommended.

## Create tables of interest

Tables concept_sets_of_interest, cohort_of_interest, visits_of_interest, metavisits_of_interest and events_of_interest were created using the data from OMOP CDMv5 tables. BuildingBlocks SQLs
were used to create these tables.

Steps to create tables of interest and insert records into them are as follows:
*  Open the PostgreSQL terminal and type the following command to create an empty schema. E.g. if your schema is 'mdcr2003_2016_selfharm' 
```
create schema mdcr2003_2016_selfharm;
```
*  Run a_create_building_blocks.sh with arguments: schema for OMOP CDMv5 database, and new schema. E.g. if your schema for OMOP CDMv5 is 'mdcr2003_2016' and your new schema is 'mdcr2003_2016_selfharm'
, type the following command on the Linux terminal: 
```
./a_create_building_blocks.sh mdcr2003_2016 mdcr2003_2016_selfharm
```
If you get 'Permission denied' error, run the following command: 
```
sh a_create_building_blocks.sh mdcr2003_2016 mdcr2003_2016_selfharm
```
OR 
change the file permissions for 'a_create_building_blocks.sh' and 'psql_run' and then run "./a_create_building_blocks.sh mdcr2003_2016 mdcr2003_2016_selfharm"
```
chmod 775 psql_run
chmod 775 a_create_building_blocks.sh
```

If you get 'Database does not exist' error, edit file *psql_run* and replace 
```psql \```
with 
```psql -d YOUR_DATABASE_NAME\```

## Data Generation for ML Models

* Run MLData SQLs "get_concept_and_ancestor.sql", "get_person_data.sql", and "get_visits_metavisits.sql" to extract the data from PostgreSQL database tables. 
* A Python-based crawler was developed to fetch the data from ICD9data.com and ICD10data.com to map ICD9PROC to ICD10PCS. Using the crawled data, a CSV (icd9_icd10Proc.csv) file 
was generated which is present in this repository. The mapping from CPT4 to ICD10PCS is not included because of the commercial license.
* File "workvariables.py" lists all input and output variables for different python programs. The file needs to be modified according to the path of those variables on the local machine.
* Run the python code "find_covariates.py" to generate the required data (data, labels, covariates) for machine learning models. 
The data are saved as a CSR matrix and labels and covariates are saved as lists.

## Machine Learning Models

Since our data are highly imbalanced, we have developed several models for the validation. Some of the models are dependent on the output of Full-data-model and Balanced-data-model. Therefore, these
two models must be executed before executing any other models.

1.  **Full-data-model:** Run the python code "classify_metavisits_all.py" to apply XGboost-based model on all meta-visits. The classification performance (MCC, AUC-ROC, and Accuracy) of the model 
is computed using 5-fold cross-validation. The classification results and the important features are used by the model to build its classification tree are saved in text files for other models 
and characterization of self-harm. There is another python code "classify_metavisits_all_n_times.py" to run the XGboost-based model on all meta-visits with 10 repetitions. In each repetition, 
the classification performance is computed using 5-fold cross-validation and then final values AUC, MCC, and Accuracy are reported at 80% confidence interval.

```
python classify_metavisits_all.py
python classify_metavisits_all_n_times.py
```

2.  **Balanced-data-model:** Run the python code "classify_metavisits_bal.py" to apply XGboost-based model on the balanced dataset comprising all 83,113 class "1" meta-visits and randomly 
selected 83,113 class "0" meta-visits. The classification performance (MCC, AUC-ROC, and Accuracy) of the model is computed using 5-fold cross-validation. The classification results and the 
important features are used by the model to build its classification tree are saved in text files. There is another program "classify_metavisits_bal_n_times.py" that applies 5 ML 
algorithms (['XGBOOST', 'DT','RF','SVM','LR']) on the balanced dataset with 100 repetitions. In each repetition, the classification performance is computed using 5-fold cross-validation
and then final values AUC, MCC and Accuracy for each classifier are reported at a 90% confidence interval.

```
python classify_metavisits_bal.py
python classify_metavisits_bal_n_times.py
```

3.  **Per-person-model:** Run the python code "classify_metavisits_per_person.py" to apply XGBoost-based model on the dataset comprising one meta-visit per person. This program uses 5-fold 
cross-validation to report the classification performance of the model.

```
python classify_metavisits_per_person.py
```

4.  **Validation-model:** Run the python code "classify_validation_set.py" to train the XGboost-based model on randomly selected 70% of all meta-visits and to validate the trained model on 
the remaining 30%. The program reports the classification performance of the model on the train set using 5-fold cross-validation as well as on the validation set.

```
python classify_validation_set.py
```

5.  **Mislabed-data-model and Mislabed-full-data-model:** Run the python code "classify_mislabeled_metavisits.py" to apply the XGboost-based model on mislabeled full data or balanced data. 
The value of the command-line argument is used to select either "full data" or "balanced data". When the "balanced data" is selected, 50% of class "1" meta-visits are randomly selected and 
their labels are flipped to "0". The ML model is executed on the data with the flipped labels and the classification performance of the model is reported using the original labels.The experiment 
is repeated by mislabeling randomly selected 50% of class "0" meta-visits. When the "full data" is selected, 50% of class "1" meta-visits are randomly selected and their labels are flipped 
to "0". The ML model is executed on the data with the flipped labels and the classification performance of the model is reported using the original labels.

```python classify_mislabeled_metavisits.py --field value; where valus is 'all' or 'bal' ```

6.  **Coding-bias-model:** Run the python code "classify_metavisits_selected_threshold.py" to apply XGboost-based model on the dataset comprising meta-visits that were predicted as 
class "1" with probability >=95% by the "Full-data-model". The classification performance is reported using 5-fold cross-validation. We used the top 100 important covariates used 
by this model to analyze the coding bias in the claims data.

```
python classify_metavisits_selected_threshold.py
```

7.  **Full-factorial-models:** Run the python code "classify_full_factorial_bal.py" to apply the XGboost-based model on the balanced data. This program takes the domain name(s) and 
ancestor (Y/N) from the command-line. It selects all the covariates of the domain(s) passed as an argument and their ancestors if 'Y' is passed for command-line parameter 'ancestor'. 
Using only those covariates, it trains and tests the model to report classification results using 5-fold cross-validation. If multiple domains are passed as the argument, it trains 
and tests the model using covariates of all possible combinations of domains and reports the classification performance of the model for each combination of domains. If no argument is 
passed, it uses covariates and their ancestors for all possible combinations of all domains to build the model. Valid domains are procedure, condition, drug, device, etc. It is better 
to run this model without any command-line argument so that the program will run for all possible combinations of all domains available in the concept table. It will also select ancestors.

If you want to run the code for all possible combinations of all domains, run the code as follows:
```
python classify_full_factorial_bal.py
```

If you want to run the code for some particular domains and also want to select ancestors, run the code as follows:
```
python classify_full_factorial_bal.py --domain domain1+domain2+...+domainN --ancestor Y
```

If you want to run the code for some particular domains and do not want to select ancestors, run the code as follows:
```
python classify_full_factorial_bal.py --domain domain1+domain2+...+domainN --ancestor N
```

## Self-harm characterization

The following python codes generate the data for self-harm incidence by gender, age, meta-visit period start year, state of residence, and MMI categories. 
These programs use the classification probability file generated by "classify_metavisits_all.py" and output files of SQL codes.

1.  **observation_period_incidence_report.py:** This code generates the data for the incidence of coded and imputed self-harm by age, meta-visit period start year, and state of residence. 
It takes age/state/year as a command-line argument to generate the data by that parameter. To run this code, type the following command: 

```
python observation_period_incidence_report.py --field value, where value is age/state/year
```

2.  **observation_period_incidence_report_gender.py:** This code generates the data for the incidence of coded and imputed self-harm by age, meta-visit period start year, and state of residence for males and females. 
It takes age/state/year as a command-line argument to generate the data by that parameter. To run this code, type the following command: 

```
python observation_period_incidence_report_gender.py --field value, where value is age/state/year
```

3.  **observation_period_incidence_report_gender_mmi.py:** This code generates the data for the incidence of coded and imputed self-harm by age, and state of residence for different MMI categories for males and females. 
It takes age/state as a command-line argument to generate the data by that parameter. To run this code, type the following command: 

```
python observation_period_incidence_report_gender_mmi.py --field value, where value is age/state
```
These 3 programs generate CSV files and the data in those CSV files are used to generate plots using XLS.