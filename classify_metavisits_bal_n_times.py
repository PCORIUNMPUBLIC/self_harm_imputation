import numpy as np
import pickle
import scipy
import os
import random
import collections
import xgboost as xgb
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.calibration import CalibratedClassifierCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import matthews_corrcoef


def load_csr_file_records():
    '''
    This function reads the CSR matrix from the saved file.
    '''
    print ('Loading large CSR file into RAM....')
    with open(wv.bal_csr_file, 'rb') as pif:
        data1 = pickle.load(pif)
        print (data1.__getattr__)
    return data1, fetch_all_labels(), fetch_all_metavisits()

def fetch_all_labels():
    '''
    This function reads the metavisit_label file returns the labels.
    '''
    print ('Fetching labels for the metavisits...')
    with open(wv.bal_labels_file) as fl:
        for line in fl:
            labels = line.rstrip().split(',')
    print ('Number of labels in the file: {0}'.format(len(labels)))
    return np.asarray(labels, dtype=np.int8)

def fetch_all_metavisits():
    '''
    This function reads metavisits file and stores metavisits in a list.
    '''
    print ('Fetching all metavisits from file and storing them in a list... ')
    with open(wv.bal_metavisits_file) as fmv:
        for line in fmv:
            metavisits = line.rstrip().split(',')
    print ('Number of metavisits in the file: {0}'.format(len(metavisits)))
    return np.asarray(metavisits)


def extract_balanced_data(X_all, Y_all, metavisits_all, Y1_idx):
    '''
    This function generates balanced data for the classifier. In each iteration,
    non_suicide metavisits are randomly selected.
    '''
    print ('\nGenerate balanced data set for the classifier.....')
    random_indices = np.random.permutation(len(Y_all)) #random record numbers
    suicide_metavisit_idx = set(Y1_idx) #convert to set
    nonsuicide_metavisit_idx = set() #use set for faster processing

    # select random indices for non_suicide metavisits
    for idx in random_indices:
        if (idx not in suicide_metavisit_idx):
            nonsuicide_metavisit_idx.add(idx)
        #count of non_suicide metavisits should be same as that of suicide metavisits
        if (len(nonsuicide_metavisit_idx) == len(suicide_metavisit_idx)):
            break

    bal_idx = list(nonsuicide_metavisit_idx.union(suicide_metavisit_idx)) # indices for the balanced data
    print ('Number of records in the balanced set...........{0}'.format(len(bal_idx)))
    return X_all[bal_idx], Y_all[bal_idx], metavisits_all[bal_idx]

def classify_the_data (X, Y, metavisits_list, classifier):
    '''
    This function calls different classifiers to classify the data and returns the classification
    results.
    '''
    print ('\nClassify the data using..... {0}'.format(classifier))
    metavisit_suicide_prob = {}
    tot = collections.Counter(Y)
    ratio = float(tot[0])/tot[1]  # value for scale_pos_weight. use scale_pos_weight as a parameter since the data have noisy labels
    print('Ratio of negative class and positive class is {0}'.format(ratio))

    # set the parameters for the classifier.
    param = {
        'max_depth': 6,
        'n_estimators': 100,
        'base_score': 0.5,
        'gamma': 0,
        'learning_rate': 0.04,
        'max_delta_step': 0,
        'min_child_weight': 2,
        'objective': 'binary:logistic',
        'booster': 'gbtree',
        'scale_pos_weight':ratio,
        'subsample': 0.6,
        'colsample_bytree': 1,
        'colsample_bylevel': 0.8,
        'n_jobs': 16,
        'missing': None,
        'random_state':0,
        'silent': 1
        }

    # compute indices for train and test data for 5-fold validation
    print ('Generating train and test indices for 5-fold validation....')
    v = np.random.permutation(X.shape[0])
    r5 = len(v) % 5
    i5 = 0
    for i in range(5):
        lv5 = int(len(v)/5)
        if(r5 > 0):
            lv5 = lv5+1
            r5 = r5-1
        teidx = v[i5:min(i5+lv5,len(v))]
        tridx = np.append(v[0:i5], v[min(i5+lv5,len(v)):len(v)])
        i5 = i5 + lv5
        print ('Performing fold number {0}'.format(i+1))
        print ('Train the classifier....')
        if (classifier == 'LR'):
            bst = linear_model.LogisticRegression().fit(X[tridx], Y[tridx]) #logistic regression
        elif (classifier == 'DT'):
            bst = DecisionTreeClassifier(random_state=0).fit(X[tridx], Y[tridx]) #decision tree
        elif (classifier == 'RF'):
            bst = RandomForestClassifier(random_state=0).fit(X[tridx], Y[tridx]) #random forest
        elif (classifier == 'SVM'):
            svm = LinearSVC()
            bst = CalibratedClassifierCV(svm).fit(X[tridx], Y[tridx]) #linear SVM
        elif (classifier == 'XGBOOST'):
            bst = xgb.XGBClassifier(**param).fit(X[tridx], Y[tridx])
            #bst = xgb.XGBClassifier(n_jobs=16).fit(X[tridx], Y[tridx])

        print ('Test the classifier....')
        preds = bst.predict_proba(X[teidx])  # this will return probabilities

        print ('Store the computed propabilities in a dictionary...')
        y_test = Y[teidx]
        for j in range(len(teidx)):
            metavisit_suicide_prob[metavisits_list[teidx[j]]] = (preds[j], y_test[j])
    return metavisit_suicide_prob


#def save_results_in_a_file (metavisit_suicide_prob, mv_probs_file):
#    '''
#    This function saves the classification results in a file for furure reference.
#    '''
#    mpf = open(mv_probs_file, 'a')
#    # write metavisit and its probabilty to an output file
#    print ('Writing metavisits and their corresponding probabilities into a file...')
#    for mvisit, pred in metavisit_suicide_prob.items():
#        line = str(mvisit) + ' : ' + str(pred[0]) +  ' : ' + str(pred[1]) + '\n'
#        mpf.write(line)

def get_accuracy_auc_mcc_score(metavisit_suicide_prob):
    '''
    This function computes the accuracy, mcc, and auc of the classifier.
    '''
    print ('\nCompute AUC, accuracy and MCC....')
    y_true = []
    y_pred_acc = []
    y_pred_auc = []
    #print(metavisit_suicide_prob)
    for mvisit, pred in metavisit_suicide_prob.items():
        y_true.append(pred[1])
        y_pred_auc.append(pred[0][1])
        y_pred_acc.append(round(pred[0][1]))
    # evaluate predictions
    acc = accuracy_score(y_true, y_pred_acc)
    mcc = matthews_corrcoef(y_true, y_pred_acc)
    auc = roc_auc_score(y_true, y_pred_auc)
    return acc, mcc, auc


def generate_report(classifier_performance):
    '''
    Using classifier_performance compute mean and deviation at 95%CI.
    '''
    for classifier, vals in classifier_performance.items():
        acc = []
        mcc = []
        auc = []
        for val in vals:
            acc.append(val[0])
            mcc.append(val[1])
            auc.append(val[2])
        acc = sorted(acc)
        mcc = sorted(mcc)
        auc = sorted(auc)
        print (classifier)
        print ('Min acc... {0}, Max acc...{1}, Mean acc...{2}, Deviation...{3}'.format(acc[4], acc[95], (acc[95]+acc[4])/2.0, (acc[95]-acc[4])/2.0))
        print ('Min mcc... {0}, Max mcc...{1}, Mean mcc...{2}, Deviation...{3}'.format(mcc[4], mcc[95], (mcc[95]+mcc[4])/2.0, (mcc[95]-mcc[4])/2.0))
        print ('Min auc... {0}, Max auc...{1}, Mean auc...{2}, Deviation...{3}'.format(auc[4], auc[95], (auc[95]+auc[4])/2.0, (auc[95]-auc[4])/2.0))


def main():
    '''
    This is main function to call other functions.
    '''
    # load large CSR matrix from .dat file
    X_all, Y_all, metavisits_all = load_csr_file_records()
    print ('Total number of metavisit with label 1 is {0}'.format(np.sum(Y_all)))
    #Y1_idx = np.where(Y_all==1)[0]  #indices of label 1

    #classify the data
    list_of_classifiers = ['XGBOOST', 'DT','RF','SVM','LR']
    classifier_performance = {}
    for i in range(wv.BAL_MV_ITERATIONS):
        print ('\nIteration number.......................{0}'.format(i))
        #X, Y, metavisits_list = extract_balanced_data(X_all, Y_all, metavisits_all, Y1_idx)
        X, Y, metavisits_list = X_all, Y_all, metavisits_all
        for classifier in list_of_classifiers:
            #mv_probs_file = metavisits_probs_file + classifier + '.txt'
            # Generate balanced data for the classifier.
            print ('Number of metavisit with label 1 in balanced set... {0}'.format(np.sum(Y)))
            # Run the classifier on the balanced data
            metavisit_suicide_prob = classify_the_data (X, Y, metavisits_list, classifier)
            # Compute the performance of the classifier
            acc, mcc, auc = get_accuracy_auc_mcc_score(metavisit_suicide_prob)
            print('Accuracy of the classifier is...............: {0}'.format(acc))
            print('MCC of the classifier is....................: {0}'.format(mcc))
            print('AUC for the classifier is...................: {0}'.format(auc))
            if (classifier in classifier_performance):
                classifier_performance[classifier].append([acc, mcc, auc])
            else:
                classifier_performance[classifier] = [[acc, mcc, auc]]
            #save_results_in_a_file (metavisit_suicide_prob, wv.bal_mv_100_iter_probs_file)
    print (classifier_performance)
    fo = open(wv.bal_mv_classifiers_performance_file, 'w')
    fo.write(str(classifier_performance)) #save the results
    generate_report(classifier_performance)
    print ('\nProgram finished successfully!!!')


################################################################################
###                            Input Output files                            ###
################################################################################
#csr_file = 'train_files/balanced_data/bal_metavisits_csr_data_2003_16.dat'
#metavisits_file = 'train_files/balanced_data/bal_metavisits_2003_16.txt'
#labels_file = 'train_files/balanced_data/bal_labels_2003_16.txt'
#metavisits_probs_file = 'classifier_outputs/metavisits_probs_using_'
#classifiers_performance_file = 'classifier_outputs/classifiers_performance_acc_mcc_auc_aug12.txt'

if __name__ == '__main__':
    '''
    This program run the classifiers 100 times and computes AUC, MCC, and Accuracy. This program may take
    several hours to finish as some of the classifiers are very slow because of large number of features.
    '''
    main()
